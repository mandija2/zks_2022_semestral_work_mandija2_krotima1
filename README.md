# ZKS_2022_semestral_work_mandija2_krotima1

Tasks
======
- [x] Napsat dokument
- [ ] Vyrobit prezentaci
- [x] Parametrized tests - registrace, login, add movie
- [x] Path based scénáře a testy
- [x] Git CI Pipeline
- [x] Vyčistit appku od testovacích dat

SUT description
=========
[App Here](https://wa.toad.cz/~krotima1/sem_final/)
