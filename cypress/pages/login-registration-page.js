class loginRegistrationPage{
    elements ={
        loginChoiceButton : () => cy.contains(' Přihlásit se '),
        registerChoiceButton : () => cy.contains(' Registrovat se '),
        loginUsernameInput : () => cy.get('[id="logname"]'),
        loginPasswordInput : () => cy.get('[id="logpass"]'),
        loginButton : () => cy.get('[id="login-btn"]'),
        loginErrorMessage : () => cy.get('[class="error"]'),
        loginOkMessage : () => cy.get('[class="success"]'),
        registerUsernameInput : () => cy.get('[id="regname"]'),
        registerEmailInput : () => cy.get('[id="regemail"]'),
        registerPhoneInput : () => cy.get('[id="regnum"]'),
        registerPasswordInput : () => cy.get('[id="regpass"]'),
        registerPasswordRepeatInput : () => cy.get('[id="regapass"]'),
        registerButton : () => cy.get('[id="register-btn"]'),
        registerErrorMessage : () => cy.get('[class="error"]'),
        registerOkMessage : () => cy.get('[class="success"]')
    }

    typeLogin(username, password){
        this.elements.loginUsernameInput().clear().then(e => { if (username !== '') cy.wrap(e).type(username) })
        this.elements.loginPasswordInput().clear().then(e => { if (password !== '') cy.wrap(e).type(password) })
    }

    login(username, password){
        this.elements.loginUsernameInput().clear().then(e => { if (username !== '') cy.wrap(e).type(username) })
        this.elements.loginPasswordInput().clear().then(e => { if (password !== '') cy.wrap(e).type(password) })
        this.elements.loginButton().click()
    }

    typeRegistration(username, email, phone, password, repeatPassword){
        this.elements.registerUsernameInput().clear().then(e => { if (username !== '') cy.wrap(e).type(username) })
        this.elements.registerEmailInput().clear().then(e => { if (email !== '') cy.wrap(e).type(email) })
        this.elements.registerPhoneInput().clear().then(e => { if (phone !== '') cy.wrap(e).type(phone) })
        this.elements.registerPasswordInput().clear().then(e => { if (password !== '') cy.wrap(e).type(password) })
        this.elements.registerPasswordRepeatInput().clear().then(e => { if (repeatPassword !== '') cy.wrap(e).type(repeatPassword) })
    }

    registration(username, email, phone, password, repeatPassword){
        this.elements.registerUsernameInput().clear().then(e => { if (username !== '') cy.wrap(e).type(username) })
        this.elements.registerEmailInput().clear().then(e => { if (email !== '') cy.wrap(e).type(email) })
        this.elements.registerPhoneInput().clear().then(e => { if (phone !== '') cy.wrap(e).type(phone) })
        this.elements.registerPasswordInput().clear().then(e => { if (password !== '') cy.wrap(e).type(password) })
        this.elements.registerPasswordRepeatInput().clear().then(e => { if (repeatPassword !== '') cy.wrap(e).type(repeatPassword) })
        this.elements.registerButton().click()
    }
}
    
    
export default new loginRegistrationPage();