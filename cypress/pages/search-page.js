class searchPage{
    elements ={
        moviesList : () => cy.get('body > div:nth-child(2) > div > div.list'),
        genreList : () => cy.get('body > div:nth-child(3) > div > div.list'),
        usersList : () => cy.get('body > div:nth-child(4) > div > div.list')
    }

    checkUsername(username){
        return cy.contains(username).should('be.visible')
    }
}
    
    
export default new searchPage();