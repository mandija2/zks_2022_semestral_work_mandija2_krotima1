class movieDeletePage{



    elements ={

        movieDeleteFormBtn :() => cy.get("body > div > div > div > a:nth-child(2)"),

        nameDeleteField : () => cy.get("#remmovie"),

        deleteBtn : () => cy.get("#remove-btn"),

        errorMessage : () => cy.get("#remove-message"),

    }

    swithcToDeleteForm(){
        this.elements.movieDeleteFormBtn().click();
    }

    typeDelete(name){
        this.elements.nameDeleteField().clear().then(e => { if (name !== '') cy.wrap(e).type(name) });
    }


    delete(name){
        this.elements.nameDeleteField().clear().then(e => { if (name !== '') cy.wrap(e).type(name) });
        this.elements.deleteBtn().click()
    }

    matchExpectedErrorMessage(error){
        return this.elements.errorMessage().should("have.class", error);
    }
}


module.exports = new movieDeletePage();
