import userPage from "./user-page.js"
import mainBar from './main-bar.js'


class accountPage{
    elements ={
        deleteButton : () => cy.get('[id="delete"]'),
    }

    checkUsername(username){
        return cy.contains(username).should('be.visible')
    }

    goToUserPage(){
        mainBar.elements.ucetButton().click();
    }

    logOutAdmin(){
        mainBar.elements.logoutButton().click();
    }

    logOut(){
        mainBar.elements.logoutButton().click();
    }l

    findUser(name){
        return userPage.findUser(name);
    }

    getBestGenre(){
        return userPage.getBestGenre();
    }

    getLikedMovies(){
        return userPage.getLikedMovies();
        
    }
    getDislikedMovies(){
        return userPage.getDislikedMovies();
    }
    
}
    
    
export default new accountPage();