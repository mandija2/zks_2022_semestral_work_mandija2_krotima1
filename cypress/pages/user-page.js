import mainBar from './main-bar.js'
import searchPage from './search-page.js';

class userPage{


    elements ={

        bestGenre : () => cy.get("#user-best-genre > div > a"),
        likedMovies : () => cy.get("#user-seen > div > a"),
        dislikedMovies : () => cy.get("#user-not-seen > div > a"),
    }

    findUser(name){
        mainBar.searchText(name);
        searchPage.elements.usersList().first().click();
    }

    getBestGenre(){
        return this.elements.bestGenre();
    }

    getLikedMovies(){
        return this.elements.likedMovies();
        
    }
    getDislikedMovies(){
        return this.elements.dislikedMovies();
    }



}


module.exports = new userPage();

