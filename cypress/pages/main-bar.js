class mainBar{
    elements ={
        logoButton : () => cy.contains('Filmátor'),
        homeButton : () => cy.contains('Home'),
        filmyButton : () => cy.contains('Filmy'),
        zanryButton : () => cy.contains('Žánry'),
        uzivateleButton : () => cy.contains('Uživatelé'),
        rezimButton : () => cy.contains('Režim'),
        loginButton : () => cy.contains('Přihlásit se'),
        searchButton : () => cy.contains('Najít'),
        searchBar : () => cy.get('[name="search"]'),
        ucetButton : () => cy.contains('Účet'),
        logoutButton : () => cy.contains('Odhlásit se'),
        addMovieButton : () => cy.contains('Přidat film')
    }

    searchText(text){
        this.elements.searchBar().clear().then(e => { if (text !== '') cy.wrap(e).type(text) })
        this.elements.searchButton().click()
    }
}
    
    
export default new mainBar();