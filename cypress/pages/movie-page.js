import mainBar from './main-bar.js'
// import 'search-page.js'
import searchPage from './search-page.js';

class moviePage{
    elements ={

        movieGenres : () => cy.get("#movie-genre > div > a"),
        movieActors : () => cy.get("#movie-actors > div"),
        movieLikedByMe : () => cy.get("#movie-user-seen > div"),
        result : () => cy.get('#movie-user-seen > div > img'),
        movieLikedByAll : () => cy.get("#movie-seen > div > a"), 
        likeBtn : () => cy.get("#movie-title > div.box-icons-wrapper > div.box-icons > a.ok"),
        dislikeBtn : () => cy.get("#movie-title > div.box-icons-wrapper > div.box-icons > a.no"),
        removeBtn : () => cy.get("#delete")
    }

    findMovie(name){
        mainBar.searchText(name);
        searchPage.elements.moviesList().first().click();
    }

    getGenres(){
        return this.elements.movieGenres();
    }

    getActors(){
        return this.elements.movieActors();
        
    }

    getLikedByMe(){
        return this.elements.movieLikedByMe();
    }

    getLikedByAll(){
        return this.elements.movieLikedByAll();
    }

    like(){
        this.elements.likeBtn().click();
    }

    dislike(){
        this.elements.dislikeBtn().click();
    }
    
    delete(){
        this.elements.removeBtn().click();
    }
}


module.exports = new moviePage();


// require("cypress-xpath")