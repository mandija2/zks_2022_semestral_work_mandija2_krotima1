class movieInsertPage{


    elements ={

        nameInsertField : () => cy.get("#addmovie"),

        genreInsertField : () => cy.get("#addgenre"),

        actorInsertField : () => cy.get("#addactors"),

        contentInsertField : () => cy.get("#addcontent"),

        addBTn : () => cy.get("#add-btn"),

        errorMessage : () => cy.get("#add-message"),

        movieInsertFormBtn : () => cy.get("body > div > div > div > a:nth-child(1)")

    }

    swithcToInsertForm(){
        this.elements.movieInsertFormBtn().click();
    }

    typeInsert(name, genres, actors, content=""){
        this.elements.nameInsertField().clear().then(e => { if (name !== '') cy.wrap(e).type(name) });
        this.elements.genreInsertField().clear().then(e => { if (genres !== '') cy.wrap(e).type(genres) })
        this.elements.actorInsertField().clear().then(e => { if (actors !== '') cy.wrap(e).type(actors) })
        this.elements.contentInsertField().clear().then(e => { if (content !== '') cy.wrap(e).type(content) })
    }


    insert(name, genres, actors, content=""){
        this.elements.nameInsertField().clear().then(e => { if (name !== '') cy.wrap(e).type(name) });
        this.elements.genreInsertField().clear().then(e => { if (genres !== '') cy.wrap(e).type(genres) })
        this.elements.actorInsertField().clear().then(e => { if (actors !== '') cy.wrap(e).type(actors) })
        this.elements.contentInsertField().clear().then(e => { if (content !== '') cy.wrap(e).type(content) })
        this.elements.addBTn().click();
    }

    matchExpectedErrorMessage(error){
        return this.elements.errorMessage().should("have.class", error);
    }
}


module.exports = new movieInsertPage();
