/// <reference types="cypress" />


//navigation and search tools to verify results
import loginRegistrationPage from '../pages/login-registration-page'
import mainBar from '../pages/main-bar'
import searchPage from '../pages/search-page'

//movie handlers
import movieInsertPage from "../pages/movie-insert-page.js"
import movieDeletePage from "../pages/movie-delete-page.js"
import moviePage from "../pages/movie-page.js"

//testdata
import users from '../fixtures/log_users.json'
import movies from '../fixtures/test_movies.json'



function get_movie(movie){
  // cy.log("see", movie.genres);
  var genres = movie.genres.length  == 1 ?  movie.genres[0] :  movie.genres.join(", ");
  // cy.log("seee", genres)
  return {"name": movie.name,"genres": genres,"actors": movie.actors,"content": movie.content};
}


  describe('Test the deletion of movie from the server', () => {
    beforeEach(() => {
      cy.visit('/')
      mainBar.elements.loginButton().click();
      loginRegistrationPage.login(users.admin.username, users.admin.password);
      mainBar.elements.addMovieButton().click();
    })

    it('Delete a non-existing test movie and check whether an error emerges', () => {
      movieDeletePage.swithcToDeleteForm();
      movieDeletePage.delete("NonExistingMovieTest");
      movieDeletePage.matchExpectedErrorMessage("error");
      movieDeletePage.elements.errorMessage().should('be.visible');

    })

    it('Delete all test movies and check whether genres with no movies were deleted', () => {
      [movies.test1, movies.test2].forEach((movieTest) => {
        const movieTestConverted = get_movie(movieTest);
        movieInsertPage.swithcToInsertForm();
        movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
        movieDeletePage.swithcToDeleteForm();
        movieDeletePage.delete(movieTestConverted.name);
      })
      var movieTest = movies.test2
      movieTest.genres.forEach((g) => {
        mainBar.searchText(g)
        searchPage.elements.genreList().first().contains(' Žádná data ').should('exist')
      });
    })

    it('Delete a test movie with a genre containing 2 movies and check whether the genre with that movie remained', () => {
      const test1 = movies.test1
      var movieTestConverted = get_movie(test1);
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
      const test2 = movies.test2
      movieTestConverted = get_movie(test2);
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)

      movieDeletePage.swithcToDeleteForm();
      movieDeletePage.delete(test2.name);
      test1.genres.forEach((g) => {
        mainBar.searchText(g)
        searchPage.elements.genreList().first().contains(' Žádná data ').should('not.exist')
      });
    })

  it('Delete a test movie and check whether the movie was deleted', () => {
    const test1 = movies.test1
    var movieTestConverted = get_movie(test1);
    movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
    movieDeletePage.swithcToDeleteForm();
    movieDeletePage.delete(test1.name);
    mainBar.searchText(test1.name)
    searchPage.elements.genreList().first().contains(' Žádná data ').should('exist')
  })


  it('Delete a movie from the movie site and check whether it was deleted', () => {
    const test1 = movies.test1
    var movieTestConverted = get_movie(test1);
    movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
    moviePage.findMovie(test1.name);
    moviePage.delete();

    mainBar.searchText(test1.name)
    searchPage.elements.genreList().first().contains(' Žádná data ').should('exist')
  })
})

  describe('Test the insertion of movie into server', () => {

    beforeEach(() => {
      cy.visit('/')
      mainBar.elements.loginButton().click();
      loginRegistrationPage.login(users.admin.username, users.admin.password);
      mainBar.elements.addMovieButton().click();
    })

    it('Add a new movie and check if movie and genre have been added', () => {
      const movieTest = movies.test1;
      const movieTestConverted = get_movie(movieTest);
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
      mainBar.searchText(movieTestConverted.name)
      searchPage.elements.moviesList().first().contains(' Žádná data ').should('not.exist')
      searchPage.elements.moviesList().first().contains(movieTestConverted.name,).should((elem) => {
            expect(elem.text()).to.equal(movieTestConverted.name,);
        });

      mainBar.searchText(movieTestConverted.genres)
      searchPage.elements.genreList().should('have.length', 1)
      searchPage.elements.genreList().first().contains(' Žádná data ').should('not.exist')
      searchPage.elements.genreList().first().contains(movieTestConverted.genres).should('exist').should((elem) => {
            expect(elem.text()).to.equal(movieTestConverted.genres);
        });
    })

    it('Add two movies with the same genre and check if the genre was added only once', () => {
      const test1 = movies.test1
      var movieTestConverted = get_movie(test1);
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
      const test2 = movies.test2
      movieTestConverted = get_movie(test2);
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)

      test2.genres.forEach((g) => {
        mainBar.searchText(g)
        searchPage.elements.genreList().should('have.length', 1)
      });
    })

    it('Add movie already existing in database, check if error has emerged',() =>{
      const movieTest = movies.test1;
      const movieTestConverted = get_movie(movieTest);
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
      movieInsertPage.elements.addBTn().trigger('mouseover');
      movieInsertPage.matchExpectedErrorMessage("error");
      movieInsertPage.elements.errorMessage().should('be.visible');
    });


    it('Add a new movie with more genres and check if all genres have been added', () => {
      const movieTest = movies.test2;
      const movieTestConverted = get_movie(movieTest);
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content);

      movieTest.genres.forEach((g) => {
        mainBar.searchText(g)
        searchPage.elements.genreList().first().contains(' Žádná data ').should('not.exist')
        searchPage.elements.genreList().first().contains(g).should((elem) => {
              expect(elem.text()).to.equal(g);
          });
      });
    })

    it('Add a movie and check its displayed genres at the movie site', () =>{
      const movieTest = movies.test2;
      const movieTestConverted = get_movie(movieTest);
      movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content);
      moviePage.findMovie(movieTest.name);
      movieTest.genres.forEach((g) =>{
        moviePage.getGenres().contains(g);
      });
    })
  })