/// <reference types="cypress" />

import mainBar from '../pages/main-bar'
import searchPage from '../pages/search-page'
import loginPage from '../pages/login-registration-page'

import login from '../fixtures/log_users.json'

describe('Path based tests TDL1', () => {
    beforeEach(() => {
        cy.visit('/')
      })
  
      it('First path', () => {
        cy.loginPathUser()
        cy.changeTheme()
        cy.checkoutUsers()
        cy.go('back')
        cy.logoutPathUser()
      })

      it('Second path', () => {
        cy.registerPathUser()
        cy.checkoutMovies()
        cy.likeDislikeMovie()
        cy.go('back')
        cy.checkoutGenres()
        cy.go('back')
        cy.checkoutUsers()
        cy.go('back')
        cy.logoutPathUser()

        cy.cleanupRegisterPathUser()
      })

      it('Third path', () => {
        cy.loginPathUser()
        cy.checkoutMovies()
        cy.go('back')
        cy.checkoutMovies()
        cy.go('back')
        cy.checkoutUsers()
        cy.go('back')
        cy.logoutPathUser()
      })


})

describe('Path based tests TDL2', () => {
  beforeEach(() => {
      cy.visit('/')
    })

    it('First path', () => {
      cy.loginPathUser()
      cy.changeTheme()
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()
    })

    it('Second path', () => {
      cy.registerPathUser()
      cy.changeTheme()
      cy.checkoutMovies()
      cy.likeDislikeMovie()
      cy.go('back')
      cy.checkoutGenres()
      cy.go('back')
      cy.logoutPathUser()

      cy.cleanupRegisterPathUser()
    })

    it('Third path', () => {
      cy.loginPathUser()
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()
    })

    it('Fourth path', () => {
      cy.registerPathUser()
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()

      cy.cleanupRegisterPathUser()
    })

    it('Fifth path', () => {
      cy.loginPathUser()
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutMovies()
      cy.likeDislikeMovie()
      cy.go('back')
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutGenres()
      cy.go('back')
      cy.checkoutGenres()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()
    })


})

describe('Path based tests TDL3', () => {
  beforeEach(() => {
      cy.visit('/')
    })

    it('First path', () => {
      cy.loginPathUser()
      cy.changeTheme()
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()
    })

    it('Second path', () => {
      cy.registerPathUser()
      cy.changeTheme()
      cy.checkoutGenres()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()

      cy.cleanupRegisterPathUser()
    })

    it('Third path', () => {
      cy.loginPathUser()
      cy.logoutPathUser()
    })

    it('Fourth path', () => {
      cy.registerPathUser()
      cy.changeTheme()
      cy.checkoutGenres()
      cy.go('back')
      cy.checkoutGenres()
      cy.go('back')
      cy.logoutPathUser()

      cy.cleanupRegisterPathUser()
    })

    it('Fifth path', () => {
      cy.loginPathUser()
      cy.changeTheme()
      cy.checkoutMovies()
      cy.likeDislikeMovie()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()
    })

    it('Sixth path', () => {
      cy.loginPathUser()
      cy.checkoutMovies()
      cy.likeDislikeMovie()
      cy.go('back')
      cy.checkoutMovies()
      cy.likeDislikeMovie()
      cy.go('back')
      cy.checkoutGenres()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()
    })

    it('Seventh path', () => {
      cy.registerPathUser()
      cy.changeTheme()
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()

      cy.cleanupRegisterPathUser()
    })

    it('Eighth path', () => {
      cy.registerPathUser()
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutMovies()
      cy.likeDislikeMovie()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()

      cy.cleanupRegisterPathUser()
    })

    it('Nineth path', () => {
      cy.loginPathUser()
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutMovies()
      cy.likeDislikeMovie()
      cy.go('back')
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutMovies()
      cy.go('back')
      cy.checkoutGenres()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.checkoutUsers()
      cy.go('back')
      cy.logoutPathUser()
    })
})
