  /// <reference types="cypress" />

//navigators
import loginRegistrationPage from '../pages/login-registration-page'
import mainBar from '../pages/main-bar'
import searchPage from '../pages/search-page'

import moviePage from "../pages/movie-page.js"
import accountPage from "../pages/account-page"

//data
import movies from '../fixtures/test_movies.json'
import users from '../fixtures/log_users.json'


describe('Test the user\'s abilities with movies' , () => {
    beforeEach(() => {
      cy.insertMovies(movies.movies_list)
      cy.clearCookies()
      cy.visit('/')
      mainBar.elements.loginButton().click();
      loginRegistrationPage.login(users.testUser.username, users.testUser.password);
    })
    afterEach(() =>{
        cy.visit('/')
        accountPage.goToUserPage();
        accountPage.logOut();
        cy.deleteMovies(movies.movies_list)
    });
  
    it('user likes the movie and checks whether the movie is in liked movies at user\'s page and the user is at movie page among other users who liked', () => {
       const testMovie = movies.test1
       moviePage.findMovie(testMovie.name)
       moviePage.like();
       moviePage.elements.result().should('be.visible');
       moviePage.getLikedByAll().contains(users.testUser.username);
       cy.checkLikedByMe(testMovie.name)
    })

    it('like 2 movies and check the expected best genre ', () => {
      const testMovie = movies.test1
      movies.movies_list.forEach((m)=>{
        moviePage.findMovie(m.name)
        moviePage.like();
        moviePage.getLikedByAll().contains(users.testUser.username);
      })
      cy.checkBestGenre("testgenre")
   })

   it('user likes and dislikes movie, check where the movie is assigned', () => {
      const testMovie = movies.test1
      moviePage.findMovie(testMovie.name)
      moviePage.like();
      moviePage.getLikedByAll().contains(users.testUser.username);
      cy.checkLikedByMe(testMovie.name)
      moviePage.findMovie(testMovie.name);
      moviePage.dislike();
      moviePage.getLikedByAll().should('not.exist');
      cy.checkDislikedByMe(testMovie.name)
  })


  it('user dislikes the movie and checks whether the movie is in disliked movies at user\'s page', () => {
    const testMovie = movies.test1
    moviePage.findMovie(testMovie.name)
    moviePage.dislike();
    moviePage.elements.result().should('be.visible');
    cy.checkDislikedByMe(testMovie.name)
 })


 //This doesnt work
//  it('dislike 2 movies and check the expected best genre is none', () => {
//   const testMovie = movies.test1
//   movies.movies_list.forEach((m)=>{
//     moviePage.findMovie(m.name)
//     moviePage.like();
//     moviePage.elements.result().should('be.visible');
//     moviePage.getLikedByAll().contains(users.testUser.username);
//   })
//   accountPage.goToUserPage();
//   accountPage.getBestGenre().should("not.exist");
// })

})


describe('Test the user\'s ability to delete account' , () => {
  beforeEach(() => {
    cy.insertMovies(movies.movies_list)
    cy.clearCookies()
    cy.visit('/')
    mainBar.elements.loginButton().click();
    loginRegistrationPage.login(users.testUser.username, users.testUser.password);
  })
  afterEach(() =>{
      cy.visit('/')
      accountPage.goToUserPage();
      accountPage.logOut();
      cy.deleteMovies(movies.movies_list)
  });

  it('like movie, delete user, register user and check whether user is no longer at movie page as well as the movie at user\'s page', () => {
    const testMovie = movies.test1
    moviePage.findMovie(testMovie.name)
    moviePage.like();
    moviePage.elements.result().should('be.visible');
    moviePage.getLikedByAll().contains(users.testUser.username);
    cy.deleteUser("");
    mainBar.elements.loginButton().click();
    loginRegistrationPage.elements.registerChoiceButton().click()
    loginRegistrationPage.registration(users.testUser.username, users.testUser.email,users.testUser.phone, users.testUser.password,  users.testUser.password)
    moviePage.findMovie(testMovie.name)
    moviePage.getLikedByAll().should('not.exist');
    accountPage.goToUserPage();
    accountPage.getLikedMovies().should('not.exist');
  })

  it('dislike movie, delete user, register user and check whether the movie is not at user\'s disliked movies', () => {
    const testMovie = movies.test1
    moviePage.findMovie(testMovie.name)
    moviePage.dislike();
    moviePage.elements.result().should('be.visible');
    cy.deleteUser("");
    mainBar.elements.loginButton().click();
    loginRegistrationPage.elements.registerChoiceButton().click()
    loginRegistrationPage.registration(users.testUser.username, users.testUser.email,users.testUser.phone, users.testUser.password,  users.testUser.password)
    accountPage.goToUserPage();
    accountPage.getDislikedMovies().should('not.exist');
  })
})
