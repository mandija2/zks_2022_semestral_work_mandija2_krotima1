/// <reference types="cypress" />

import loginRegistrationPage from '../pages/login-registration-page'
import mainBar from '../pages/main-bar'
import accountPage from '../pages/account-page'

import login from '../fixtures/login_comb.json'
import existingUsers from '../fixtures/log_users.json'

describe('Parametrized login form tests', () => {
  beforeEach(() => {
    cy.visit('/')
    mainBar.elements.loginButton().click()
  })

  it('Valid login form input combinations', () => {
    const validUsers = login.valid
    validUsers.forEach((user) => {
      loginRegistrationPage.elements.loginChoiceButton().click()
      loginRegistrationPage.typeLogin(user.username, user.password)
      loginRegistrationPage.elements.loginButton().trigger('mouseover')
      loginRegistrationPage.elements.loginOkMessage().should('be.visible')
    })
  })

  it('Invalid login form input combinations', () => {
    const invalidUsers = login.invalid
    invalidUsers.forEach((user) => {
      var username = user.username
      var password = user.password

      if(username == "empty"){
        username = ''
      } 
      if(password == "empty"){
        password = ''
      }

      loginRegistrationPage.elements.loginChoiceButton().click()
      loginRegistrationPage.typeLogin(username, password)
      loginRegistrationPage.elements.loginButton().click()
      loginRegistrationPage.elements.loginErrorMessage().should('be.visible')

    })
  })
})

describe('Non parametrized login form tests', () => {
  beforeEach(() => {
    cy.visit('/')
    mainBar.elements.loginButton().click()
  })

  it('Not existing valid user cannot log in', () => {
    loginRegistrationPage.elements.loginChoiceButton().click()
    loginRegistrationPage.typeLogin('validButNonExistant', 'validPassword')
    loginRegistrationPage.elements.loginButton().click()
    loginRegistrationPage.elements.loginErrorMessage().should('be.visible')
  })

  it('Existing users can log in with their account and log out', () => {
    const users = existingUsers.validUsers
    users.forEach((user) => {
      loginRegistrationPage.login(user.username, user.password)
      mainBar.elements.ucetButton().click()
      accountPage.checkUsername(user.username)
      mainBar.elements.logoutButton().click()
      mainBar.elements.logoButton().click()
      loginRegistrationPage.elements.loginChoiceButton().click()
    })
  })

})