/// <reference types="cypress" />

import mainBar from '../pages/main-bar'
import searchPage from '../pages/search-page'

describe('Parametrized registration form tests', () => {
    beforeEach(() => {
        cy.visit('/')
      })
  
      it('Find non existant string', () => {
          var text = "xwxwx"
          
          mainBar.searchText(text)

          searchPage.elements.moviesList().should('have.length', 1)
          searchPage.elements.moviesList().first().should('contain', ' Žádná data ')
          searchPage.elements.genreList().should('have.length', 1)
          searchPage.elements.genreList().first().should('contain', ' Žádná data ')
          searchPage.elements.usersList().should('have.length', 1)
          searchPage.elements.usersList().first().should('contain', ' Žádná data ')

      })

      it('Find elements beginning with existant string', () => {
        var text = "a"
        
        mainBar.searchText(text)

        searchPage.elements.moviesList().first().contains(' Žádná data ').should('not.exist')
        searchPage.elements.moviesList().first().contains(/^[Aa]/).should('exist')

        searchPage.elements.genreList().first().contains(' Žádná data ').should('not.exist')
        searchPage.elements.genreList().first().contains(/^[Aa]/).should('exist')

        searchPage.elements.usersList().first().contains(' Žádná data ').should('not.exist')
        searchPage.elements.usersList().first().contains(/^[Aa]/).should('exist')
    })


})
