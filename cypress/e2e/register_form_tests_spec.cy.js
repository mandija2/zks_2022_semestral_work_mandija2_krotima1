/// <reference types="cypress" />

import loginRegistrationPage from '../pages/login-registration-page'
import mainBar from '../pages/main-bar'
import accountPage from '../pages/account-page'

import register from '../fixtures/register_comb.json'
import existingUsers from '../fixtures/log_users.json'

describe('Parametrized registration form tests', () => {
  beforeEach(() => {
    cy.visit('/')
    mainBar.elements.loginButton().click()
    loginRegistrationPage.elements.registerChoiceButton().click()
  })

  it('Invalid registration form input combinations', () => {
    const invalidCombinations = register.invalid
    invalidCombinations.forEach((user) => {
        var username = user.username
        var email = user.email
        var phone = user.phone
        var password = user.password

        if(username == "empty"){
            username = ''
        }

        if(email == "empty"){
            email = ''
        }

        if(phone == "empty"){
            phone = ''
        }

        if(password == "empty"){
            phone = ''
        }

        loginRegistrationPage.elements.registerChoiceButton().click()
        loginRegistrationPage.registration(username, email, phone, password, password)
        loginRegistrationPage.elements.registerErrorMessage().should('be.visible')
    })
  })

  it('Valid registration form input combinations', () => {
    const validCombinations = register.valid
    validCombinations.forEach((user) => {
        var username = user.username
        var email = user.email
        var phone = user.phone
        var password = user.password

        if(username == "empty"){
            username = ''
        }

        if(email == "empty"){
            email = ''
        }

        if(phone == "empty"){
            phone = ''
        }

        if(password == "empty"){
            phone = ''
        }

        loginRegistrationPage.elements.registerChoiceButton().click()
        loginRegistrationPage.typeRegistration(username, email, phone, password, password)
        loginRegistrationPage.elements.registerButton().trigger('mouseover')
        loginRegistrationPage.elements.registerOkMessage().should('be.visible')
    })
  })

  it('Valid registration users cannot register with wrong repeated password', () => {
    const validCombinations = register.valid
    validCombinations.forEach((user) => {
        var username = user.username
        var email = user.email
        var phone = user.phone
        var password = user.password

        loginRegistrationPage.elements.registerChoiceButton().click()
        loginRegistrationPage.registration(username, email, phone, password, "thisIsNotTheSamePassword")
        loginRegistrationPage.elements.registerErrorMessage().should('be.visible')
    })
  })

  it('Valid registration users cannot register with already existing username', () => {
      const existing = existingUsers.validUsers
      existing.forEach((user) => {
          var username = user.username
          var email = "validemail@email.cz"
          var phone = "123456789"
          var password = "legitPassword"
  
          loginRegistrationPage.elements.registerChoiceButton().click()
          loginRegistrationPage.registration(username, email, phone, password, password)
          loginRegistrationPage.elements.registerErrorMessage().should('be.visible')
      })
    })
  
})

describe('Other registration form tests', () => {
    beforeEach(() => {
      cy.visit('/')
      mainBar.elements.loginButton().click()
      loginRegistrationPage.elements.registerChoiceButton().click()
    })

    it('Valid registration user can register, view their account with correct username and delete their account', () => {
        var username = "tmpTestUser"
        var email = "validemail@email.cz"
        var phone = "123456789"
        var password = "legitPassword"

        loginRegistrationPage.elements.registerChoiceButton().click()
        loginRegistrationPage.registration(username, email, phone, password, password)
        mainBar.elements.ucetButton().click()
        accountPage.checkUsername(username)
        accountPage.elements.deleteButton().click()
        mainBar.elements.loginButton().click()
        loginRegistrationPage.login(username, password)
        loginRegistrationPage.elements.loginErrorMessage().should('be.visible')

    })
  

  
    
  })