/// <reference types="cypress" />

import loginRegistrationPage from "../pages/login-registration-page.js"
import movieInsertPage from "../pages/movie-insert-page.js"
import mainBar from '../pages/main-bar'
import searchPage from '../pages/search-page'

import movies_combs from '../fixtures/addmovie_comb.json'
import users from '../fixtures/log_users.json'




function fieldNameAssigner(name){
  switch(name) {
    case "empty":
      return "";
    case "correct_multi":
      return "name1, name, name!";
    case "correct_single":
      return "name1?!";
    default:
      return name;
  }
}

function fieldActorsAssigner(name){
  switch(name) {
    case "empty":
      return "";
    case "correct_multi":
      return "actorA, actorB, actorC";
    case "correct_single":
      return "actor";
    default:
      return name;
  }
}

function fieldGenreAssigner(genre){
  switch(genre) {
    case "empty":
      return "";
    case "correct_genres":
      return "genreA, genreB, genreC";
    case "correct_genre":
      return "genre";
    default:
      return genre;
  }
}

function fieldContentAssigner(content){
  switch(content) {
    case "empty":
      return "";
    case "anything":
      return "This is the content of this movie?!:)";
    default:
      return content;
  }
}


function testConvertor(movie){
    return [fieldNameAssigner(movie.name), fieldGenreAssigner(movie.genre), fieldActorsAssigner(movie.actors), fieldContentAssigner(movie.content)];
}

  describe('Parametrized movie insertion form tests', () => {

    beforeEach(() => {
      cy.visit('/')
      mainBar.elements.loginButton().click();
      loginRegistrationPage.login(users.admin.username, users.admin.password);
      mainBar.elements.addMovieButton().click();
    })
  
  
    it('Check valid movie insertions', () => {
      
      var valid_movies = movies_combs.valid
      valid_movies.forEach((movie) => {
        const convertedInput = testConvertor(movie);
        movieInsertPage.typeInsert(convertedInput[0], convertedInput[1], convertedInput[2],convertedInput[3]);
        movieInsertPage.elements.addBTn().trigger('mouseover')
        movieInsertPage.matchExpectedErrorMessage("success");
        movieInsertPage.elements.errorMessage().should('be.visible')
      })
    })

    it('Check invalid movie insertions', () => {
      
      var valid_movies = movies_combs.invalid
      valid_movies.forEach((movie) => {
        var convertedInput = testConvertor(movie);
        movieInsertPage.typeInsert(convertedInput[0], convertedInput[1], convertedInput[2],convertedInput[3]);
        movieInsertPage.elements.addBTn().trigger('mouseover')
        movieInsertPage.matchExpectedErrorMessage("error");
        movieInsertPage.elements.errorMessage().should('be.visible')
      })
    })
  
  })















