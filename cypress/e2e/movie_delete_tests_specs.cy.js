/// <reference types="cypress" />

import loginRegistrationPage from "../pages/login-registration-page.js"
import movieDeletePage from "../pages/movie-delete-page.js"
import mainBar from '../pages/main-bar'

import movies_combs from '../fixtures/deletemovie_comb.json'
import users from '../fixtures/log_users.json'


function fieldNameAssigner(name){
  switch(name) {
    case "empty":
      return "";
    case "correct_multi":
      return "name1, name?, name!";
    case "correct_single":
      return "name1?!";
    default:
      return name;
  }
}

function testConvertor(name){
  return fieldNameAssigner(name);
}

describe('Parametrized movie deletion form tests', () => {

    beforeEach(() => {
      cy.visit('/')
      mainBar.elements.loginButton().click();
      loginRegistrationPage.login(users.admin.username, users.admin.password);
      mainBar.elements.addMovieButton().click();
      movieDeletePage.swithcToDeleteForm();
    })
  
  
    it('Check valid movie deletions', () => {
      
      var valid = movies_combs.valid
      valid.forEach((n) => {
        cy.log(n)
        const name = testConvertor(n.name);
        movieDeletePage.typeDelete(name);
        movieDeletePage.elements.deleteBtn().trigger('mouseover')
        movieDeletePage.matchExpectedErrorMessage("success");
        movieDeletePage.elements.errorMessage().should('be.visible')
      })
    })

    it('Check invalid movie deletions', () => {
      
      var invalid = movies_combs.invalid
      invalid.forEach((n) => {
        const name = testConvertor(n.name);
        movieDeletePage.typeDelete(name);
        movieDeletePage.elements.deleteBtn().trigger('mouseover')
        movieDeletePage.matchExpectedErrorMessage("error");
        movieDeletePage.elements.errorMessage().should('be.visible')
      })
    })
  
})