// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

//Mock Admin add movies
import users from '../fixtures/log_users.json'
import registration from '../fixtures/register_users.json'
//navigators
import loginRegistrationPage from '../pages/login-registration-page'
import mainBar from '../pages/main-bar'
import accountPage from '../pages/account-page'

//movie handlers
import movieInsertPage from "../pages/movie-insert-page.js"
import movieDeletePage from "../pages/movie-delete-page.js"

Cypress.Commands.add("insertMovies", (movies) =>{
    cy.visit('/')
    mainBar.elements.loginButton().click();
    loginRegistrationPage.login(users.admin.username, users.admin.password);
    mainBar.elements.addMovieButton().click();
    movies.forEach((movie)=>{
        const movieTestConverted = {"name": movie.name,"genres": movie.genres.length  == 1 ?  movie.genres[0] :  movie.genres.join(", "),"actors": movie.actors,"content": movie.content};;
        movieInsertPage.swithcToInsertForm();
        movieInsertPage.insert(movieTestConverted.name, movieTestConverted.genres, movieTestConverted.actors,movieTestConverted.content)
    })
    accountPage.goToUserPage();
    accountPage.logOutAdmin();
})

Cypress.Commands.add("deleteMovies", (movies) =>{
    cy.visit('/')
    mainBar.elements.loginButton().click();
    loginRegistrationPage.login(users.admin.username, users.admin.password);
    mainBar.elements.addMovieButton().click();
    movieDeletePage.swithcToDeleteForm();
    movies.forEach((movie)=>{
        movieDeletePage.delete(movie.name)
    })
    accountPage.goToUserPage();
    accountPage.logOutAdmin();
})

Cypress.Commands.add("loginPathUser", () =>{
    cy.visit('/')
    mainBar.elements.loginButton().click();
    loginRegistrationPage.login(users.pathUser.username, users.pathUser.password);
    mainBar.elements.homeButton().click();
})

Cypress.Commands.add("logoutPathUser", () =>{
    cy.visit('/')
    mainBar.elements.ucetButton().click()
    mainBar.elements.logoutButton().click()
})

Cypress.Commands.add("registerPathUser", () =>{
    const user = registration.pathRegistrationUser
    cy.visit('/')
    mainBar.elements.loginButton().click()
    loginRegistrationPage.elements.registerChoiceButton().click()
    loginRegistrationPage.registration(user.username, user.email, user.phone, user.password, user.password)
    mainBar.elements.homeButton().click()

})

Cypress.Commands.add("cleanupRegisterPathUser", () =>{
    const user = registration.pathRegistrationUser
    cy.visit('/')
    mainBar.elements.loginButton().click()
    loginRegistrationPage.login(user.username, user.password)
    mainBar.elements.ucetButton().click()
    accountPage.elements.deleteButton().click()
})

Cypress.Commands.add("checkoutUsers", () =>{
    mainBar.elements.uzivateleButton().click()
    
    cy.get('#list > a:nth-child(1)').click()
    cy.get('#user-name > div.box-item.box-name').should('be.visible')
    cy.go('back')

    cy.get('#pagination > a:nth-child(2)').click()
    cy.get('#list > a:nth-child(1)').click()
    cy.get('#user-name > div.box-item.box-name').should('be.visible')
    cy.go('back')
    cy.go('back')
})

Cypress.Commands.add("checkoutMovies", () =>{
    mainBar.elements.filmyButton().click()
    
    cy.get('#list > a:nth-child(1)').click()
    cy.get('#movie-title > div.box-item.box-name').should('be.visible')
    cy.go('back')

    cy.get('#pagination > a:nth-child(3)').click()
    cy.get('#list > a:nth-child(8)').click()
    cy.get('#movie-title > div.box-item.box-name').should('be.visible')
    cy.go('back')
    cy.go('back')
})

Cypress.Commands.add("checkoutGenres", () =>{
    mainBar.elements.zanryButton().click()
    
    cy.get('#list > a:nth-child(4)').click()
    cy.get('body > main > div.list-tittle > h2').should('be.visible')
    cy.get('#list').should('be.visible')
    cy.go('back')

})

Cypress.Commands.add("likeDislikeMovie", () =>{
    cy.get('#list > a:nth-child(1)').click()
    cy.get('#movie-title > div.box-icons-wrapper > div.box-icons > a.ok').click()
    cy.get('#movie-user-seen > div > img').should('be.visible')
    cy.get('#movie-title > div.box-icons-wrapper > div.box-icons > a.no').click()
    cy.get('#movie-user-seen > div > img').should('be.visible')
    cy.go('back')

})

Cypress.Commands.add("changeTheme", () =>{
    mainBar.elements.rezimButton().click()
})

Cypress.Commands.add("deleteUser", (user) =>{
    cy.visit('/')
    mainBar.elements.ucetButton().click()
    accountPage.elements.deleteButton().click()
})

Cypress.Commands.add("checkLikedByMe", (movie) =>{
    cy.visit('/')
    accountPage.goToUserPage();
    accountPage.getLikedMovies().contains(movie);
    cy.go("back")
})
Cypress.Commands.add("checkDislikedByMe", (movie) =>{
    cy.visit('/')
    accountPage.goToUserPage();
    accountPage.getDislikedMovies().contains(movie);
    cy.go("back")
})

Cypress.Commands.add("checkBestGenre", (genre) =>{
    cy.visit('/')
    accountPage.goToUserPage();
    accountPage.getBestGenre().contains(genre);
    cy.go("back")
})