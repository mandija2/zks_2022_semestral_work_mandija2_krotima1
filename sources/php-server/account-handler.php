<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php


include_once "session-handler.php";



 /**
  * Class checks logged in users, based on that it renreds possible options. 
  * Admin       = can add movies, delete movies as well as genres and delete users.
  * logged user = can like movies also dislike, and log in or log out.
  * basic user  = can browse the web and register or log in.
  *
  */

 class account_handler{

    private int $id;

    public function __construct(){}


    public function user_generate_login(){
        $curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
        if (isset($_SESSION["user"])){
            if ($curPageName == "user.php" && isset($_GET["id"]) && session_get_logged_id() == $_GET["id"]){
                echo '
                    <a href="php-server/user-logout-execute.php?page='.$curPageName .'?id='.$_SESSION["user"].' ">
                        <div class="login-wrapper">
                            <h2 class= "main-tool button-color">Odhlásit se</h2>
                        </div>
                    </a>
                ';
            }else{
                echo '
                        <a href="user.php?id='.$_SESSION["user"].' ">
                            <div class="login-wrapper">
                                <h2 class= "main-tool button-color">Účet</h2>
                            </div>
                        </a>
                    ';
            }
        }else{
            echo '
                    <a href="login.php">
                        <div class="login-wrapper">
                            <h2 class= "main-tool button-color">Přihlásit se</h2>
                        </div>
                    </a>
                ';
        }
    }


    public function user_generate_edit_button(){
        $curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
        if ($curPageName == "user.php" && isset($_GET["id"]) && is_numeric($_GET["id"]) && session_get_logged_id() == $_GET["id"]){
            echo '
            <a href="login.php">
                <div class="login-wrapper">
                    <h2 class= "main-tool button-color">Edit</h2>
                </div>
            </a>
            ';
        }
    }


    public function admin_generate_add_movie(){
        if (session_get_admin()){
            echo   '<a href="add_movie.php">
                        <div class="login-wrapper">
                            <h2 class= "main-tool button-color">Přidat film</h2>
                        </div>
                    </a>';
         }
    }
}


?>