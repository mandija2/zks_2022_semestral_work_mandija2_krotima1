<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

/**
 * functions that handle session
 * 
 */
function session_set_up(){
    if (!isset($_SESSION)){
        session_start();
    }
    if (!isset($_SESSION["style"])){
        $_SESSION["style"] = 0;
    }
}

function session_get_logged_id(){
    if (isset($_SESSION["user"])){
        return $_SESSION["user"];
    }
    return -1;
}

function session_get_admin(){
    if (isset($_SESSION["admin"])){
        return $_SESSION["admin"];
    }
    return false;
}

function session_log_in($user){
    $_SESSION["user"] = $user["id"];
    $_SESSION["admin"] = false;
    if (isset($user["admin"])){
        $_SESSION["admin"] = true;
    }
}

function session_log_out(){
    unset($_SESSION["user"]);
    unset($_SESSION["admin"]);
}

function session_change_style(){
    $_SESSION["style"] = 1 - $_SESSION["style"]; 
}

?>