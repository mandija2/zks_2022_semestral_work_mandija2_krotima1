<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

require_once "php-server/json-handler.php";


/**
 * Class to prepare html code of items list to browse
 *      
 * @ Properties
 *      path                :the path to a file from which the data will be loaded
 *      curr_website:       :the actual website to keep focus.
 *      additional_params   :additional parameters to pass in the url of the actual website
 *      target_website      :the name of website to redirect to if the item of list was clicked
 *      target_params       :additional parameters to pass in the url of the target website
 *      max_size_per_page   :the number of items in the list per page
 *      isApproved          :true or false variable indicating if all required args were given in the url 
 */
class List_Handler{
    public string $path;
    public string $curr_website;
    public string $additional_params;
    public string $target_website;
    public string $target_params;
    public int    $max_size_per_page;
    public bool   $isApproved;
    public bool   $noArgs;

    // constructor
    public function __construct(string $path,string $curr_website,
         string $target_website, string $target_params="", string $additional_params="", int $max_size_per_page=13){
        $this->path = $path;
        $this->curr_website = $curr_website;
        $this->target_website = $target_website;
        $this->target_params=$target_params;
        $this->additional_params=$additional_params;
        $this->max_size_per_page = $max_size_per_page;
        $this->isApproved = false; //set to false, needs to be validated
        $this->noArgs = false;
    }

    // validate expected args in get and its size, it is required to run this function after creating new instance
    public function validate_GET_args(array $arr){
        $expected = sizeof($arr);
        if ($expected != 0){
            //size
            if($expected != sizeof($_GET)){
                return $this->isApproved;
            }
            //names of args
            foreach($_GET as $key => $it){
                if(!in_array($key, $arr)){
                    return $this->isApproved;
                }
            }
        }else{
            $this->noArgs = true;
        }
        $this->isApproved = true;
        return  $this->isApproved;
    }

    // switches between labels A/Z and Z/A based on chosen sorting by user
    public function change_sorting_labels(){
        if ($this->isApproved){
            $curr_page = (int)$_GET["page"];
            $curr_sort = $_GET["sort"];
            
            // switch if
            $sorting_name = "A/Z";
            if (strcmp($curr_sort,'z') == 0){
                $sorting_name = "Z/A";
                $curr_sort = "a";
            }else{
                $curr_sort = "z";
            }
            
            // print A/Z or Z/A
            echo '<a href="'.$this->curr_website.'.php?page='.$curr_page.
            '&sort='.$curr_sort.''.$this->additional_params.' " class="sorting" >'.$sorting_name.'</a>';
        }
    }


    // fills the list with items and adds each of them link to required website
    public function fill_list_with_items(callable $filter_fc = null){
        $data = load_data_json($this->path);
        $size = sizeof($data);

        // check whether all required arguments were given
        if ($this->isApproved){
            // if the function was given, filter the required data
            $data = $filter_fc == null ? $data : $filter_fc($data);
            // sort
            $data = $this->sort_data($data);
            $size = sizeof($data);
            if ($size > 0){
                // Number of pages based on size of data and max number of items per page
                $num_pages = ceil($size / $this->max_size_per_page);

                // get current page and handle extreme cases
                $curr_page = $this->clamp($this->noArgs ? 1 : (int)$_GET["page"], 1, $num_pages);

                // loop through data and get only n items per page
                $iter = 0;
                foreach($data as $key=>$val){
                    if ($iter == $this->max_size_per_page*$curr_page){
                        break;
                    }
                    if($iter >= $this->max_size_per_page*($curr_page-1) && $iter < $size){
                        $item = $data[$key];
                        echo '<a class="list_item" href="'.$this->target_website.'.php?id='.$item['id'].''.$this->target_params.' ">'.htmlspecialchars($item['name']).'</a>';
                    }
                    $iter = (int)$iter + 1;

                }
                //add pagination
                if (!$this->noArgs){
                    $this->add_pagination($num_pages, $curr_page);
                }
            }else{
                echo '<a class="list_item"> Žádná data </a>';
            }
            
        }else{
            // loop through all data, if some of the required arguments was not appeared
            foreach($data as $key=>$val){
                $item = $data[$key];
                echo '<a class="list_item" href="'.$this->target_website.'.php?id='.$item['id'].''.$this->target_params.' ">'.htmlspecialchars($item['name']).'</a>';
            }

        }

    }

    // add pagination in the bottom part of page.
    private function add_pagination(int $num_pages, int $curr_page){
        $curr_sort = htmlspecialchars($_GET["sort"]);
        echo "<div id=pagination>";
        for ($i = 0; $i < $num_pages; $i++){
            if ($i+1 == $curr_page){
                // set focus to index of current page
                echo '<a href="'.$this->curr_website.'.php?page='.($i+1).'&sort='.$curr_sort.''.$this->additional_params.' " id="page_focus" class="page_item">'.($i+1).'</a>';
            }else{
                // add indexes of pages
                echo '<a href="'.$this->curr_website.'.php?page='.($i+1).'&sort='.$curr_sort.''.$this->additional_params.' " class="page_item">'.($i+1)."</a>";
            }
        }
        echo "</div>";
    }



    public function cmpZ($a, $b){
        $key = 'name';
        return -1*strcmp($a[$key],$b[$key]);
    }

    public function cmpA($a, $b){
        $key = 'name';
        return strcmp($a[$key],$b[$key]);
    }

    // sort data based on chosen sorting
    private function sort_data(array $data) : array{
        if (!$this->noArgs){
            $curr_sort = $_GET["sort"];
            if (strcmp($curr_sort,'z') == 0){
                // function cmp($a, $b){
                //     $key = 'name';
                //     return -1*strcmp($a[$key],$b[$key]);
                // }
                usort($data,  function($a, $b) {return -1*strcmp($a["name"],$b["name"]);});
            }else{
                // function cmp($a, $b){
                //     $key = 'name';
                //     return strcmp($a[$key],$b[$key]);
                // }
                usort($data, function($a, $b) {return strcmp($a["name"],$b["name"]);});
            }
        }
        return $data;
    }





    // clamp variable in the interval
    private function clamp($var, $min, $max){
        return $var < $min ? $min : ($var > $max ? $max : $var);
    }

}
?>