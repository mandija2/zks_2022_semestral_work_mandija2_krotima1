<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

require_once "php-server/json-handler.php";


/**
 * Base class for window rendering: Movie and User
 *  returns content of current object
 * 
 */
class Window_Handler{
    public string $path;
    public string $curr_website;
    public bool   $isApproved;
    public string $target_column;
    public $data = null;
    public $curr_data = null;

    // constructor
    public function __construct(string $path, string $curr_website, string $target_column="id"){
        $this->path = $path;
        $this->curr_website = $curr_website;
        $this->target_column = $target_column;
        $this->isApproved = false; //set to false, needs to be validated
    }

    // validate expected args in get and its size, it is required to run this function after creating new instance
    public function validate_GET_args(array $arr) :bool{
        $expected = sizeof($arr);
        //size
        if($expected != sizeof($_GET)){
            return $this->isApproved;
        }
        //names of args
        foreach($_GET as $key => $it){
            if(!in_array($key, $arr)){
                return $this->isApproved;
            }
        }
        $this->isApproved = true;
        return  $this->isApproved;
    }

    //init data variable to keep focus, it is required to be initialized after validate_GET function
    public function init_data(callable $filter_fc=null) :bool{
        if ($this->isApproved){
            $target_col_data = $_GET[$this->target_column];
            
            // load data
            $items = load_data_json($this->path);
            $items = $filter_fc == null ? $items : $filter_fc($items);
            $size = sizeof($items);
            
            $keys = array_keys($items);
            for ($i = 0; $i < $size; $i++){
                $item = $items[$keys[$i]];
                if (strcmp($item[$this->target_column],$target_col_data) == 0){
                    $prev_i = ($i == 0 ? $size-1 : $i-1);
                    $next_i = ($i == $size-1 ? 0 : $i+1);
                    $this->data = array($items[$keys[$prev_i]], $items[$keys[$i]],$items[$keys[$next_i]]);
                    $this->curr_data =  $items[$keys[$i]];
                    return true;
                }
            
            }
        }
        return false;
    }


    // returns link to next page
    public function get_right_link(){
        if ($this->data == null){
            echo '<a href="'.$this->curr_website.'.php" class="box-switch-wrapper">';
        }else{
            echo '<a href="'.$this->curr_website.'.php?'.$this->target_column.'='.$this->data[2][$this->target_column].'" class="box-switch-wrapper">';
        }
    }

    // returns link to next page
    public function get_left_link(){
        if ($this->data == null){
            echo '<a href="'.$this->curr_website.'.php" class="box-switch-wrapper">';
        }else{
            echo '<a href="'.$this->curr_website.'.php?'.$this->target_column.'='.$this->data[0][$this->target_column].'" class="box-switch-wrapper">';
        }
    }

    // returns content based on given key from data,
    // if both the link and the get_element function is provided it returns link to some page
    public function get_content($key, string $link = null, callable $get_element = null){
        if ($this->curr_data != null){

            if (isset($this->curr_data[$key])){
                if (is_array($this->curr_data[$key])){
                    foreach($this->curr_data[$key] as $k => $i){
                        $this->print_element($i, $link, $get_element);
                    }
                }else{
                    $this->print_element($this->curr_data[$key], $link, $get_element);
                }
                return;
            }
        }
        echo htmlspecialchars($key);
    }


    // print element
    private function print_element($element,string $link = null, callable $get_element = null){
        if ($link !=null && $get_element != null){
            $entity = $get_element($this->curr_data,$element);
            echo '<a class="box-item-field box-item-link" href="'.$link.''.$entity["id"].' " >'.htmlspecialchars($entity["name"]).'</a>,  ';
        }else{
            echo htmlspecialchars($element);
        }
    }

}

?>