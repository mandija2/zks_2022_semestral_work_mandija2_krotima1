<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

require_once "php-server/window-handler.php";
include_once "php-server/session-handler.php";
include_once "php-server/entity-manager.php";

/**
 * Movie window render
 *  basicaly returns content of movie
 * 
 */
class movie_window_handler extends Window_Handler{


    // constructor
    public function __construct(string $path, string $curr_website, string $target_column="id"){
        Window_Handler::__construct($path, $curr_website, $target_column);
    }


    public function get_favourite_genre(){
        if ($this->curr_data != null){
            if (isset($this->curr_data["genre_likes"]) && sizeof($this->curr_data["genre_likes"])>0){
                asort($this->curr_data["genre_likes"]);
                $best = end($this->curr_data["genre_likes"]);
                $entity = (new entity_manager)->merge_entity("data/genres.json","name", key($best));
                echo '<a class="box-item-field box-item-link" href="genre.php?page=1&sort=a&id='.$entity["id"].' " >'.htmlspecialchars($entity["name"]).'</a>,  ';
                return;
            }
        }
        echo '<div class="box-item-field"> - </div>';
    }


    // prints like buttons
    public function generate_like_buttons(){
        if ($this->isApproved && $this->curr_data!=null){

            if (isset($_GET["id"]) && is_numeric($_GET["id"]) && !session_get_admin()){
                if (session_get_logged_id() != -1 ){
                    echo ' <div class="box-icons-title">Viděl jsi?</div>
                            <div class="box-icons">';
                    echo '<a href="php-server/like_movie_execute.php?id='.$this->curr_data["id"].'&like=1" class="ok"> <img alt="videl" src="resources/ok.svg"> </a>
                        <a href="php-server/like_movie_execute.php?id='.$this->curr_data["id"].'&like=0" class="no"> <img alt="nevidel" src="resources/no.svg"> </a>';
                    echo ' </div>';
                }else{
                    echo ' <div class="box-icons-title">Viděl jsi?</div>
                            <div class="box-icons">';
                    echo '      <a href="login.php" class="ok"> <img alt="videl" src="resources/ok.svg"> </a>
                                <a href="login.php" class="no"> <img alt="nevidel" src="resources/no.svg"> </a>';
                    echo ' </div>';
                }

                return;
            }elseif(isset($_GET["id"]) && is_numeric($_GET["id"]) && session_get_admin()){
                echo ' <div class="box-icons-title">Odstranit film?</div>
                         <div class="box-icons">';
                echo '        <a href="php-server/delete_movie_execute.php?id='.$this->curr_data["id"].'"  id="delete" class="no"> <img alt="nevidel" src="resources/no.svg"> </a>';
                echo '   </div>';
            }
        }
        return;
   }

    // prints liked image
    public function generate_liked(){
        if ($this->isApproved && $this->curr_data != null){
            // if the user liked or disliked this film it will show 
            if (is_numeric($_GET["id"]) && session_get_logged_id() != -1 && !session_get_admin()){
                $user = (new entity_manager)->merge_entity("data/users.json", "id", session_get_logged_id());
                $movie_id = $this->curr_data["id"];
                if (in_array($movie_id,$user["like"])){
                    echo '<div class="box-item-field"> <img alt="stav" src="resources/ok.svg"> </div>';
                }else if(in_array($movie_id,$user["dislike"])){
                    echo '<div class="box-item-field"> <img alt="stav" src="resources/no.svg"> </div>';
                }
                else{
                    echo '<div class="box-item-field"> - </div>';
                }
                return;
            }
        }
        echo '<div class="box-item-field"> - </div>';
        return;
    }
}


?>