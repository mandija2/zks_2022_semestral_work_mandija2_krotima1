<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

include "entity-manager.php";
include "session-handler.php";

/**
 * like or dislike movie by logged user, this code is executed
 * only if the this link is active.
 * 
 */
$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
session_set_up();


/** 
 * deletes user's account by the user itself
 * 
 */
function clear_user($entity){
    foreach($entity["like"] as $key => $movie){
        $tmp = (new entity_manager())->merge_entity("../data/movies.json", "id", $movie);
        (new entity_manager())->remove_element_entity("../data/movies.json", $tmp, "seen", $entity["id"]);
    }
    foreach($entity["genre_likes"] as $genre => $count){
        $tmp = (new entity_manager())->merge_entity("../data/genres.json", "name", $genre);
        $value = ((int)$tmp["likes"]-(int)$count);
        (new entity_manager())->update_entity("../data/genres.json", $tmp, "likes", $value);
    }
}

/**
 * like movie
 * 
 */
if ($curPageName == "like_movie_execute.php" && session_get_logged_id()!=-1 && !session_get_admin()
     && isset($_GET["id"]) && is_numeric($_GET["id"]) && isset($_GET["like"]) && $_GET["like"]== 1 ){
    $id = session_get_logged_id();
    $movie_id = (int)$_GET["id"];
    $user = (new entity_manager)->merge_entity('../data/users.json',"id", $id);
    $movie = (new entity_manager)->merge_entity('../data/movies.json',"id", $movie_id);
    //update user
    if (!in_array($id,$movie["seen"])){
        $user = (new entity_manager)->remove_element_entity('../data/users.json', $user,"dislike",$movie_id);
        $user = (new entity_manager)->update_entity('../data/users.json', $user,"like", $movie_id);
        foreach ($movie["genre"] as $genre_name){
            $user = (new entity_manager)->update_entity('../data/users.json', $user,"genre_likes", 1, $genre_name);
            //update genre
            $genre = (new entity_manager)->merge_entity('../data/genres.json',"name",$genre_name);
            (new entity_manager)->update_entity('../data/genres.json', $genre,"likes", (int)$genre["likes"]+1);
        }
        
        //update movie
        $movie = (new entity_manager)->update_entity('../data/movies.json', $movie,"seen",$id);
    }

   header('Location: ../movie.php?id='.$movie_id);
}


/**
 * dislike movie
 * 
 */
if ($curPageName == "like_movie_execute.php" && session_get_logged_id()!=-1 && !session_get_admin()
     && isset($_GET["id"]) && is_numeric($_GET["id"]) && isset($_GET["like"]) && $_GET["like"]== 0 ){
    $id = session_get_logged_id();
    $movie_id = (int)$_GET["id"];
    $user = (new entity_manager)->merge_entity('../data/users.json',"id",$id);
    $movie = (new entity_manager)->merge_entity('../data/movies.json',"id",$movie_id);

    if (!in_array($movie_id,$user["dislike"])){
        //update user
        $user = (new entity_manager)->remove_element_entity('../data/users.json', $user,"like",$movie_id);
        $user = (new entity_manager)->update_entity('../data/users.json', $user,"dislike", $movie_id);
        foreach ($movie["genre"] as $genre_name){
            $user = (new entity_manager)->update_entity('../data/users.json', $user,"genre_likes", -1, $genre_name);
            //update genre
            $genre = (new entity_manager)->merge_entity('../data/genres.json',"name",$genre_name);
            (new entity_manager)->update_entity('../data/genres.json', $genre,"likes", (int)$genre["likes"]-1);
        }
        
        //update movie
        $movie = (new entity_manager)->remove_element_entity('../data/movies.json', $movie,"seen",$id);
    }
    header('Location: ../movie.php?id='.$movie_id);
}

//header('Location: /index.php');


?>