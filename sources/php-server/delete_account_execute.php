<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

include "entity-manager.php";
include "session-handler.php";

/**
 * delte user and return to the previous page, this code is executed
 * only if the this link is active.
 * 
 */
$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
session_set_up();


function clear_user($entity){
    foreach($entity["like"] as $key => $movie){
        $tmp = (new entity_manager())->merge_entity("../data/movies.json", "id", $movie);
        (new entity_manager())->remove_element_entity("../data/movies.json", $tmp, "seen", $entity["id"]);
    }
    foreach($entity["genre_likes"] as $genre => $count){
        $tmp = (new entity_manager())->merge_entity("../data/genres.json", "name", $genre);
        $value = ((int)$tmp["likes"]-(int)$count);
        (new entity_manager())->update_entity("../data/genres.json", $tmp, "likes", $value);
    }
}


/** 
 * deletes user's account by the user itself
 * 
 */
if ($curPageName == "delete_account_execute.php" && session_get_logged_id()!=-1 && !session_get_admin()){
    $id = session_get_logged_id();
    $entity = (new entity_manager)->merge_entity('../data/users.json',"id",$id);
    if ($entity != null){
        //clear_user($entity);
        ///(new entity_manager())->delete_entity("../data/users.json", $entity);
        (new entity_manager())->delete_user($entity);
        
    }
    //logout user
    session_log_out();
    header('Location: ../index.php');
}

/** 
 * deletes user's account by the admin
 * 
 */
if ($curPageName == "delete_account_execute.php" && session_get_admin() && isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id = (int)$_GET["id"];
    if ($id != (int)session_get_logged_id()){
        $entity = (new entity_manager)->merge_entity('../data/users.json',"id",$id);
        if ($entity != null){
            // clear_user($entity);
            // (new entity_manager())->delete_entity("../data/users.json", $entity);
            (new entity_manager())->delete_user($entity);
        }
        //keep logged id after shifting ids
        $admin = (new entity_manager)->merge_entity("../data/users.json", "name", "admin");
        session_log_in($admin);
    }
    header('Location: ../movies.php?page=1&sort=a');
}
header('Location: ../index.php');


?>