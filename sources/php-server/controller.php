<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

    include_once "php-server/entity-manager.php";
    include_once "php-server/session-handler.php";



/**
 * Base form controller
 * 
 * properties
 *      expected        : array of expected args in post method
 *      isApproved      : bool indicating if all expected args were sent
 *      target_website  : page to redirect
 *      err_print       : holds error message
 * 
 */
abstract class controller{

    public array  $expected;
    public bool   $isApproved = false;
    public string $target_website;
    public string $err_print ="";

    // validates given array in post, if a mistake occurs prints error message.
    public abstract function validate();

    // validates an input based on its argument
    public abstract function validate_input($arg, $input);

    // get post if form was sent
    public function get_arg_value($name){ 
        if ($this->isApproved){
            echo htmlspecialchars($_POST[$name]); 
        }
    }

    // validates expected args in post and its size
    public function validate_POST_args() :bool{
        $size = sizeof($this->expected);
        //size
        if($size != sizeof($_POST)){
            return $this->isApproved;
        }
        //names of args
        foreach($_POST as $key => $it){
            if(!in_array($key, $this->expected)){
                return $this->isApproved;
            }
        }
        $this->isApproved = true;
        return  $this->isApproved;
    }

    // checks whether the input contains only letters or digits
    protected function check_chars_nums($input, $emessage){
        if (!preg_match('/^([ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9]*)$/', $input)){
            return $emessage;
        }
        return "";
    }

    // checks whether the input contains only digits
    protected function check_number($input, $emessage){
        if (!preg_match("/^[0-9]*$/", $input)){
            return $emessage;
        }
        return "";
    }

    // checks whether the input has certain size
    protected function check_exact_size($input, $emessage, $size){
        if (strlen($input) != $size){
            return $emessage;
        }
        return "";
    }

    // checks whether the input has required size
    protected function check_min_size($input, $emessage, $size){
        if (strlen($input) < $size){
            return $emessage;
        }
        return "";
    }

    protected function check_empty($input){
        if (strlen($input)==0){
            return "Všechna pole musí být vyplněna. <br>";
        }
        return "";
    }

    // checks whether the input has expected structure
    protected function check_chars_nums_space($input, $emessage){
        if (!preg_match('/^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9,.!?# ]+$/', $input)){
            return $emessage;
        }
        return "";
    }

    // checks whether the input has expected structure
    protected function check_chars_space_commas($input, $emessage){
        if (!preg_match('/^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z, ]+$/', $input)){
            return $emessage;
        }
        return "";
    }
}




/**
 *  Register form controller
 */
class register_controller extends controller{

    public string $path = "data/users.json";
    public int    $min_len_pass = 4;
    public int    $min_len_name = 4;
    public array  $error_message = array( 
                                    "name" => "Jméno musí mít pouze písmena a čísla. <br> ",
                                    "n_size" => 'Jméno musí mít alespoň 4 znaky. <br> ',
                                    "email" => "Email musí mít tvar: _@_.__. <br> ",
                                    "number" => "Tel. číslo musí obsahovat pouze čísla. <br> ",
                                    "num_size" => "Tel. číslo musí být dlouhé 9 znaků. <br> ",
                                    "password" => "Hesla se neshodují. <br>",
                                    "p_size" => "Heslo musí mít alespoň 4 znaky. <br> "
                                );

    function __construct(array $expected, string $target_website){
        $this->expected = $expected;
        $this->target_website = $target_website;
        $this->validate_POST_args();
    }


    // validates given input in post, if a mistake occurs prints error message.
    public function validate(){
        if ($this->isApproved){
            $e_message ="";

            foreach ($_POST as $arg => $value){
                $this->err_print = $this->err_print.$this->validate_input($arg,  $value);
            }



            $this->err_print = $this->err_print.$this->check_passwords($_POST["password"],$_POST["apassword"],$this->error_message["password"]);
            
            if ($this->err_print != ""){
                return;
            }

            $this->err_print=$this->err_print.$this->check_existing_account("name",$_POST["name"]);
            $this->err_print=$this->err_print.$this->check_existing_account("email",$_POST["email"]);
            if ($this->err_print != ""){
                return;
            }
            
            
            $new_entity = ["name" => $_POST["name"],"email" => $_POST["email"],"number" => $_POST["number"],"password" => $_POST["password"]];
            
            $id = (new entity_manager())->create_user($new_entity);
            session_log_in(["id" => $id]);
            $_POST = array();
            header('Location: '.$this->target_website.'' .$new_entity["name"]);
        }
    }

    // checks individual input, returns error message
    public function validate_input($arg, $input) : string{
        if ($arg == "name"){
            return $this->check_chars_nums($input, $this->error_message["name"])
                    .$this->check_min_size($input, $this->error_message["n_size"],$this->min_len_name);
        }elseif($arg == "email"){
            return $this->check_email($input, $this->error_message["email"]);
        }elseif($arg == "number"){
            return $this->check_number($input, $this->error_message["number"])
                    .$this->check_exact_size($input, $this->error_message["num_size"],9);
        }elseif($arg == "password"){
            return $this->check_min_size($input, $this->error_message["p_size"], $this->min_len_pass);
        }elseif($arg == "apassword"){
            return $this->check_min_size($input, $this->error_message["p_size"], $this->min_len_pass);
        }
        return "";
    }

    // checks email if is in correct form
    private function check_email($input, $emessage){
        if (!filter_var($input, FILTER_VALIDATE_EMAIL)){
            return $emessage;
        }
        return "";
    }


    // checks equality of passwords, returns error message
    private function check_passwords($pass, $apass, $emessage){
        if ($pass != $apass){
            return $emessage;
        }
        return "";
    }

    // checks whether the account hasnt been already created, if yes returns error message
    private function check_existing_account($key, $name){
        $user = (new entity_manager)->merge_entity($this->path, $key, $name);
        if (sizeof($user)>0){
            return "Účet s ".htmlspecialchars($key).": ".htmlspecialchars($name)." již existuje. <br>";
        }
        return "";
    }

    // prints error message
    public function print_error_message(){
        echo "<p id= 'error-message' class='error'>".$this->err_print."</p>";
    }


}


/**
 *  login form controller
 * 
 */

class login_controller extends controller{
    public string $path = "data/users.json";
    public int    $min_len_pass = 4;
    public int    $min_len_name = 4;
    public array  $error_message = array( 
                                    "name" => "Jméno musí mít pouze písmena a čísla. <br> ",
                                    "n_size" => 'Jméno musí mít alespoň 4 znaky. <br> ',
                                    "p_size" => "Heslo musí mít alespoň 4 znaky. <br> ",
                                    "exist"  => "Jméno nebo heslo je nesprávné. <br>"
                                );

    function __construct(array $expected, string $target_website){
        $this->expected = $expected;
        $this->target_website = $target_website;
        $this->validate_POST_args();
    }

    // validates given input in post, if a mistake occurs prints error message.
    public function validate(){
        if ($this->isApproved){
            foreach ($_POST as $arg => $value){
                $this->err_print = $this->err_print.$this->validate_input($arg,  $value);
            }
            if ($this->err_print != ""){
                return;
            }

            $new_entity = ["name" => $_POST["name"],"password" => $_POST["password"]];
            $user = (new entity_manager())->authenticate_user($new_entity);
            
            if ($user == null){
                $this->err_print = $this->error_message["exist"];
                return;
            }
            session_log_in($user);
            $_POST = array();
            header('Location: '.$this->target_website.'' . $user["name"]);
        }
    }

    // checks individual input, returns error message
    public function validate_input($arg, $input) : string{
        if ($arg == "name"){
            return $this->check_chars_nums($input, $this->error_message["name"])
                    .$this->check_min_size($input, $this->error_message["n_size"],$this->min_len_name);
        }elseif($arg == "password"){
            return $this->check_min_size($input, $this->error_message["p_size"], $this->min_len_pass);
        }
        return "";
    }
    
    // prints error message
    public function print_error_message(){
        echo "<p id= 'login-error-message' class='error'>".$this->err_print."</p>";
    }

}



class add_movie_controller extends controller{
    public string $path_movie = "data/movies.json";
    public string $path_genre = "data/genres.json";
    public int    $min_len_genre = 1;
    public int    $min_len_name = 1;
    public array  $error_message = array( 
                                    "movie" => "Název filmu musí mít pouze písmena, čísla znaky ,.?! nebo mezery. <br> ",
                                    "genre" => 'Název žánrů musí obsahovat pouze písmena čárku pro oddělení a mezeru. <br> ',
                                    "actors" => "Herci musí obsahovat pouze písmena nebo čísla. <br> ",
                                    "exist"  => "Tento film již existuje. <br>",
                                    "success"  => "Film byl úspěšně vytvořen.",
                                    "size" => "Název filmu, žánru nebo herců nesmí být prázndý.  <br> "
                                );
    public string $err_print ="";

    function __construct(array $expected, string $target_website){
        $this->expected = $expected;
        $this->target_website = $target_website;
        $this->validate_POST_args();
    }

    // validates given input in post, if a mistake occurs prints error message.
    public function validate(){
        if ($this->isApproved){
            foreach ($_POST as $arg => $value){
                $this->err_print = $this->err_print.$this->validate_input($arg,  $value);
            }
            if ($this->err_print != ""){
                return;
                //echo "<p id= 'add-message' class='error'>".$e_message."</p>";
            }

            $new_entity = ["name" => $_POST["name"],"genre" => $_POST["genre"],"actors" => $_POST["actors"],"content" => $_POST["content"]];
            $movie = (new entity_manager())->merge_entity($this->path_movie, "name", $new_entity["name"]);
            if (sizeof($movie) > 0){
                //echo "<p id= 'add-message' class='error'>".$this->error_message["exist"]."</p>";
                $this->err_print = $this->error_message["exist"];
                return;
            }
            (new entity_manager())->create_movie($new_entity);
            //echo "<p id= 'add-message' class='success'>".$this->error_message["success"]."</p>";
           
            header('Location: '.$this->target_website.'?success=' .$this->error_message["success"]);
        }
    }

    // checks individual input, returns error message
    public function validate_input($arg, $input) : string{
        if ($arg == "name"){
            return $this->check_chars_nums_space($input, $this->error_message["movie"])
                    .$this->check_min_size($input, $this->error_message["size"],$this->min_len_name);
        }elseif($arg == "genre"){
            return $this->check_chars_space_commas($input,$this->error_message["genre"])
                    .$this->check_min_size($input, $this->error_message["size"], $this->min_len_name);
        }elseif($arg == "actors"){
            return $this->check_chars_space_commas($input,$this->error_message["actors"])
                    .$this->check_min_size($input, $this->error_message["size"], $this->min_len_name);
        }
        return "";
    }

    public function print_error_message(){
        echo "<p id= 'add-message' class='error'>".$this->err_print."</p>";
    }
                            

}

class delete_movie_controller extends controller{
    public string $path_movie = "data/movies.json";
    public string $path_genre = "data/genres.json";
    public int    $min_len_genre = 1;
    public int    $min_len_name = 1;
    public array  $error_message = array( 
                                    "movie" => "Název filmu musí mít pouze písmena, čísla znaky ,.?! nebo mezery. <br> ",
                                    "exist"  => "Tento film neexistuje. <br>",
                                    "success"  => "Film byl úspěšně odstraněn.",
                                    "size" => "Název filmu nesmí být prázndý.  <br> "
                                );
    public string $err_print ="";

    function __construct(array $expected, string $target_website){
        $this->expected = $expected;
        $this->target_website = $target_website;
        $this->validate_POST_args();
    }

    // validates given input in post, if a mistake occurs prints error message.
    public function validate(){
        if ($this->isApproved){
            foreach ($_POST as $arg => $value){
                $this->err_print = $this->err_print.$this->validate_input($arg,  $value);
            }
            if ($this->err_print != ""){
                return;
            }

            $to_delete_entity = ["name" => $_POST["name"]];
            $movie = (new entity_manager())->merge_entity($this->path_movie, "name", $to_delete_entity["name"]);
            if (sizeof($movie) == 0){
                //echo "<p id= 'remove-message' class='error'>".$this->error_message["exist"]."</p>";
                $this->err_print =  $this->error_message["exist"];
                return;
            }
            (new entity_manager())->delete_movie($movie);
            //echo "<p id= 'remove-message' class='success'>".$this->error_message["success"]."</p>";
            
            header('Location: '.$this->target_website.'?success=' .$this->error_message["success"]);
        }
    }

    // checks individual input, returns error message
    public function validate_input($arg, $input) : string{
        if ($arg == "name"){
            return $this->check_chars_nums_space($input, $this->error_message["movie"])
                    .$this->check_min_size($input, $this->error_message["size"],$this->min_len_name);
        }
        return "";
    }


    public function print_error_message(){
        echo "<p id= 'remove-message' class='error'>".$this->err_print."</p>";
    }                    

}






?>