<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

include_once "session-handler.php";

/**
 * sets session style and return to the previous page, this code is executed
 * only if the this link is active.
 * 
 */
$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
if ($curPageName == "style-handler.php" && isset($_GET["page"])){
    session_set_up();
    session_change_style(); 
    header('Location: '.substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "page=/") +5));
}

// returns which colors will be loaded
function get_style_file(){
    if ($_SESSION["style"] == 0){
        echo '<link rel="stylesheet" href="css-styles/dark-theme/colors.css">';
    }else{
        echo '<link rel="stylesheet" href="css-styles/light-theme/colors.css">';
    }
}


?>