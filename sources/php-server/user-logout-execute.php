<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

include_once "session-handler.php";

/**
 * log out user and return to the previous page, this code is executed
 * only if the this link is active.
 * 
 */
$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);

if ($curPageName == "user-logout-execute.php" && isset($_GET["page"])){
    session_set_up();
    session_log_out();
    header('Location: ../'.$_GET["page"]);
}
header('Location: ../'.$_GET["page"]);


?>
