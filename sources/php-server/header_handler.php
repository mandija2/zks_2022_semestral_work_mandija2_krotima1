<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php


include_once "php-server/session-handler.php";
include_once "php-server/account-handler.php";
session_set_up();





function generate_header($curPageName){
    $ac_handler = new account_handler();

    echo '<header>
    <div class="main-header-wrapper">
        <div class="main-wrapper">
            <a href="index.php">
            <div class="logo-wrapper">
                <h1 class="logo logo-color">Filmátor</h1>
            </div>
            </a>
            <div class="main-tools" >';
                $ac_handler->admin_generate_add_movie();
                echo '<a href="php-server/style-handler.php?page='.$_SERVER['REQUEST_URI'].'" >
                    <div class="style-wrapper">
                        <h2 class= "main-tool button-color">Režim</h2>
                    </div>
                </a>';
                $ac_handler->user_generate_login();
    echo    '</div>
        </div>
    </div>
    <nav class="toolbar-header-wrapper">
        <div class="toolbar-searchbar-wrapper">
            <div class="toolbar-wrapper">
                <a href="index.php"><h3 class="toolbar-item button-color">Home</h3></a>
                <a href="movies.php?page=1&sort=a"><h3 class="toolbar-item button-color">Filmy</h3></a>
                <a href="genres.php?page=1&sort=a"><h3 class="toolbar-item button-color">Žánry</h3></a>
                <a href="users.php?page=1&sort=a"><h3 class="toolbar-item button-color">Uživatelé</h3></a>
            </div>
                <form action="search.php" class="searchbar ">
                    <input type="hidden"  name="page" value="1">
                    <input type="hidden"  name="sort" value="a">
                    <input type="text" placeholder="Hledej..." name="search" required>
                    <button type="submit" class="searchbar-button button-color">Najít</button>
                </form>
        </div>
    </nav>
</header>';

}

?>