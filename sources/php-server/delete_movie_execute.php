<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

include "entity-manager.php";
include "session-handler.php";

/**
 * delet movie by admin user, this code is executed
 * only if the this link is active.
 * 
 */
$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
session_set_up();

if ($curPageName == "delete_movie_execute.php" && session_get_logged_id()!=-1 && session_get_admin()
     && isset($_GET["id"]) && is_numeric($_GET["id"])){
    $movie = (new entity_manager)->merge_entity("../data/movies.json", "id", $_GET["id"]);
    if(sizeof($movie)>0){
        (new entity_manager)->delete_movie($movie, "../");

    }
    header('Location: ../movies.php?page=1&sort=a');

}


 ?>
