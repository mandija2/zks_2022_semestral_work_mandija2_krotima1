<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

// paths to files with data
$genres_path = "data/genres.json";
$movies_path = "data/movies.json";

// loads data from a json file and returns them represented in array 
function load_data_json(string $path) : array{
    // Read data from json file
    $string = file_get_contents($path);
    // convert read data to json structure
    $list = json_decode($string, true);
    return $list;
}


// saves the adday in json file
function save_data_json(string $path, array $array){
    $string = json_encode($array);
    file_put_contents($path, $string);
}

function save_updated_data_json(string $path, array $array){
    //load json data
    $list = load_data_json($path);
    foreach ($list as $key=>$item){
        if ($item["id"] ==  $array["id"]){
            $list[$key] = $array;
            break;
        }
    }
    save_data_json($path, $list);
}

function add_new_element_to_json(string $path, array $element){
    //load json data
    $list = load_data_json($path);
    //set id
    $element["id"] = sizeof($list) == 0 ? 0 : (end($list)["id"]+1);
    //add new element
    array_push($list, $element);
    //save updated array
    save_data_json($path, $list);
    return $element["id"];
}

?>