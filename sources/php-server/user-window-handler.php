<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

require_once "php-server/window-handler.php";
include_once "php-server/session-handler.php";
include_once "php-server/entity-manager.php";

/**
 * Render user window
 *  basicaly returns content of user
 * 
 */

class user_window_handler extends Window_Handler{
    // constructor
    public function __construct(string $path, string $curr_website, string $target_column="id"){
        Window_Handler::__construct($path, $curr_website, $target_column);
    }

    // print personal info
    public function get_personal_info(){
        // if the user is exactly on his account or admin can see more info
        if ($this->curr_data != null && isset($_GET["id"]) && is_numeric($_GET["id"]) && (session_get_logged_id() == $_GET["id"] || $this->curr_data != null && session_get_admin())){
            echo '<div id="user-phone" class = "box-items">
                <span   class="box-item movie-content" >
                    Telefonní číslo
                </span>
                <div class="box-item-field">'.htmlspecialchars($this->curr_data["number"]).'
                </div>
            </div>';
            echo '<div id="user-email" class = "box-items">
                <span   class="box-item movie-content" >
                    Email
                </span>
                <div class="box-item-field">'.htmlspecialchars($this->curr_data["email"]).'
                </div>
            </div>';
        }
       
    }


    public function get_favourite_genre(){
        if ($this->curr_data != null){
            if (isset($this->curr_data["genre_likes"]) && sizeof($this->curr_data["genre_likes"])>0){
                asort($this->curr_data["genre_likes"], SORT_NUMERIC);
                if (end($this->curr_data["genre_likes"])>0){
                    $best_genre_name = array_key_last($this->curr_data["genre_likes"]);
                    $genre = (new entity_manager)->merge_entity("data/genres.json","name", $best_genre_name);
                    echo '<a class="box-item-field box-item-link" href="genre.php?page=1&sort=a&id='.$genre["id"].' " >'.htmlspecialchars($genre["name"]).'</a>,  ';
                }else{
                    echo '<div class="box-item-field"> - </div>';
                }
                return;
            }
        }
        echo '<div class="box-item-field"> - </div>';
    }


    // prints delete button
    public function generate_delete_account_button(){
        if ($this->isApproved){
            // if the user is admin he can delete all users except himself
            if (isset($_GET["id"]) && is_numeric($_GET["id"]) && session_get_admin() && $_GET["id"]!=session_get_logged_id()){
            echo '<div class="box-icons-title">Odstranit účet?</div>
                    <div class="box-icons">
                            <a href="php-server/delete_account_execute.php?id='.$_GET["id"].' "  id="delete" class="no"> <img alt="seen" src="resources/no.svg"></a>
                </div>';
            return;
            }

            // if the user is exactly on his account except admin
            if (isset($_GET["id"]) && is_numeric($_GET["id"]) && session_get_logged_id() == $_GET["id"] && !session_get_admin()){
            echo '   <div 
                            class="box-icons-title">Odstranit účet?
                        </div>
                        <div class="box-icons">
                            <a href="php-server/delete_account_execute.php" id="delete" class="no"> <img alt="seen" src="resources/no.svg"></a>
                        </div>';
            return;
            }
        }
        return;
   }
}


?>