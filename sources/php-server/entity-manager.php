<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

include_once "json-handler.php";


/***
 *  Class works on entities. In the web there are only three entities: User, Movie and Genre
 *  
 *  It ensures easier access to the json files, basicly it works with some arrays representing
 *  each entity and provides functions that encapsulates the operations over entities and json files
 * 
 * 
 * 
 */
class entity_manager{

    // creates new entity user and return its id
    public static function create_user(array $user){
        //salt the password
        $user["password"] = password_hash($user["password"].''.$user["name"], PASSWORD_DEFAULT, array('cost' => 10));
        
        $user["like"] = [];
        $user["dislike"] = [];
        $user["genre_likes"] = [];
        return add_new_element_to_json("data/users.json", $user);
        //Mandarinka 245[08]
    }

    // authenticate user
    public static function authenticate_user(array $user){
        $data = load_data_json("data/users.json");
        foreach ($data as $item){
            if ($item["name"] == $user["name"] && password_verify($user["password"].''.$user["name"], $item["password"])){
                return $item;
            }
        }
        return null;
    }

    //delete user account
    public static function delete_user(array $user){
        //remove all occurrences of user in seen movies
        foreach($user["like"] as $key => $movie){
            $tmp = (new entity_manager())->merge_entity("../data/movies.json", "id", $movie);
            (new entity_manager())->remove_element_entity("../data/movies.json", $tmp, "seen", $user["id"]);
        }
        //remove likes from user in genres
        foreach($user["genre_likes"] as $genre => $count){
            $tmp = (new entity_manager())->merge_entity("../data/genres.json", "name", $genre);
            $value = ((int)$tmp["likes"]-(int)$count);
            (new entity_manager())->update_entity("../data/genres.json", $tmp, "likes", $value);
        }
        //delete user
        self::delete_entity("../data/users.json", $user);
    }

    //create new movie
    public static function create_movie(array $movie){
        //remove spaces and separete by ,
        $string = str_replace(' ', '', $movie["genre"]);
        $genres_names = explode(",", $string);
        //set genres
        $movie["genre"] = $genres_names;
        $movie["seen"] = [];
        //save new movie and get id
        $movie_id = add_new_element_to_json("data/movies.json", $movie);

        //update already existing genres by new movie if it contains the genre
        $genres = load_data_json("data/genres.json");
        foreach($genres as $genre){
            if (in_array($genre["name"],$genres_names)){
                if (($key = array_search($genre["name"], $genres_names)) !== false) {
                    unset($genres_names[$key]);
                }
                self::update_entity("data/genres.json", $genre, "movies", $movie_id);
            }
            if (sizeof($genres_names)==0){
                break;
            }
        }
        //create new genres
        if (sizeof($genres_names)>0){
            foreach($genres_names as $genre_name){
                $add_genre = ["name" => $genre_name, "movies"=>[$movie_id]];
                self::create_genre($add_genre);
            }
        }
        return $movie_id;
    }

    //create genre
    public static function create_genre(array $genre){
        $genre["likes"] = 0;
        return add_new_element_to_json("data/genres.json", $genre);
    }


    // delete movie
    public static function delete_movie(array $movie,string $prefix = ""){
        $users = load_data_json($prefix."data/users.json");
        $genres = load_data_json($prefix."data/genres.json");
        // loop through genres and if the genre contains the movie, remove it
        foreach($genres as $genre){
            if (in_array($genre["name"], $movie["genre"])){
                $genre = self::remove_element_entity($prefix."data/genres.json", $genre, "movies", $movie["id"]);
                // loop through users and remove liked or disliked movies
                foreach($users as $user){
                    if (in_array($movie["id"], $user["like"])){
                        $user = self::remove_element_entity($prefix."data/users.json", $user, "like", $movie["id"]);
                    }elseif(in_array($movie["id"], $user["dislike"])){
                        $user = self::remove_element_entity($prefix."data/users.json", $user, "dislike", $movie["id"]);
                    }
                    // if the genre contains no movies remove it from user
                    if (sizeof($genre["movies"])==0){
                        self::remove_element_entity($prefix."data/users.json", $user, "genre_likes" ,$genre["name"]);
                    }
                }
            }
        }
        // remove genre if has no movies
        //$counter = 0;
        foreach($genres as $genre){
            if (in_array($genre["name"], $movie["genre"]) && sizeof($genre["movies"])==1){
                //$genre["id"] = (int)$genre["id"] - (int)$counter;
                self::delete_entity($prefix."data/genres.json",$genre);
                //$counter = $counter + 1;
            }
        }
        //remove movie
        self::delete_entity($prefix."data/movies.json",$movie);

    }



    // merge entity with record in json file
    public static function merge_entity(string $path, string $key, $value) : array{
        $data = load_data_json($path);
        foreach ($data as $item){
            if ($item[$key] == $value){
                return $item;
            }
        }
        return array();
        return null;
    }

    // get all entities
    public static function get_all(string $path){
        $data = load_data_json($path);
        return $data;
    }

    // update entity by value in current key
    public static function update_entity(string $path, $entity, string $key, $value, $add_key=null){
        if (is_array($entity[$key])){
            if ($add_key ==null){
                array_push($entity[$key],$value);
            }else{
                // setting dict in array
                $tmp = $entity[$key];
                if(isset($entity[$key][$add_key])){
                    $entity[$key][$add_key] = $entity[$key][$add_key] + $value;
                }else{
                    $entity[$key][$add_key] = $value;
                }
            }

        }else{
            $entity[$key] = $value;
        }
        save_updated_data_json($path, $entity);
        return $entity;
    }

    // remove attribute in current key from entity
    public static function remove_element_entity(string $path, $entity, string $key, $value){
        if (is_array($entity[$key])){
            if (in_array($value, $entity[$key])) 
            {
                unset($entity[$key][array_search($value,$entity[$key])]);
            }elseif(in_array($value,array_keys($entity[$key]))){
                unset($entity[$key][$value]);
            }else{
                return $entity;
            }
        }
        save_updated_data_json($path, $entity);
        return $entity;
    }

    // delete entity
    public static function delete_entity(string $path, $entity){
        $data = load_data_json($path);
        $wasUnset = false;
        $prevkey = null;
        foreach ($data as $key => $value){
            // if ($wasUnset){
            //     $data[$key]["id"] = $data[$key]["id"]-1;
            //     $data[$prevkey] = $data[$key];
            //     $prevkey = $key;
            //     unset($data[$key]);
            // }
            if (isset($data[$key]["id"]) && $data[$key]["id"] == $entity["id"] && !$wasUnset){
                unset($data[$key]);
                $prevkey = $key;
                $wasUnset = true;
                break;
            }
        }
        save_data_json($path,$data);
    }



}

?>