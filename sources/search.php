<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

include "php-server/session-handler.php";
include "php-server/style-handler.php";
include "php-server/list-handler.php";
include "php-server/header_handler.php";

// set up session
session_set_up();

//get current page name
$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);  


//check expected argument
function check_get_ID(){
    return isset($_GET["search"]);
}

$isApprovedID = check_get_ID();

//set variables
$path_movie = "data/movies.json";
$website = "search";
$target_website_movie = "movie";
$additional_par =  $isApprovedID ? "&search=".htmlspecialchars($_GET["search"]) : "";
$max_size_per_page = 5;
$expected_arg = ["page","sort", "search"];

$path_genre = "data/genres.json";
$target_website_genre = "genre";
$target_par_genre ="&sort=a&page=1";

$path_user = "data/users.json";
$target_website_user = "user";


//create list handlers
$movie_handler = new List_Handler($path = $path_movie, $curr_website=$website, $target_website= $target_website_movie, $target_params="",$additional_params=$additional_par , $max_size_per_page = $max_size_per_page);
$movie_handler->validate_GET_args($expected_arg);

$genre_handler = new List_Handler($path = $path_genre, $curr_website=$website, $target_website = $target_website_genre, $target_params=$target_par_genre,$additional_params=$additional_par , $max_size_per_page = $max_size_per_page);
$genre_handler->validate_GET_args($expected_arg);

$user_handler = new List_Handler($path = $path_user, $curr_website=$website, $target_website = $target_website_user, $target_params="",$additional_params=$additional_par , $max_size_per_page = $max_size_per_page);
$user_handler->validate_GET_args($expected_arg);

// filter data by given string 
function filter_name($data){
    if (isset($_GET["search"])){
        $prefix = $_GET["search"];
        return array_filter($data, function($a) use ($prefix){return strtolower(substr($a["name"], 0, (strlen($prefix) > strlen($a["name"]) ? strlen($a["name"]) : strlen($prefix)))) == strtolower($prefix);});
    }else{
        return $data;
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="js-scripts/header.js"></script>
    <link rel="stylesheet" href="css-styles/header-style.css"/>
    <link rel="stylesheet" href="css-styles/body-index-style.css"/>
    <link rel="stylesheet" href="css-styles/list-style.css"/>
    <link rel="stylesheet" href="css-styles/footer-style.css"/>
    <?php get_style_file() ?>
    <link rel="stylesheet" media="print" href="css-styles/print/list.css"/>
    <title>Filmator</title>
</head>

<body class="first-section">
   <?php generate_header($curPageName);?>
   
        <div class="main-list-wrapper">
            <div class="list-wrapper">
                <div class="list-tittle">
                    <?php if($movie_handler->isApproved){echo "<a></a>";} ?>
                    <h2> Filmy </h2>
                    <?php $movie_handler->change_sorting_labels() ?>
                </div>
                <div class="list">
                    <?php $movie_handler->fill_list_with_items("filter_name"); ?>
                </div>
            </div>
        </div>
        <div class="main-list-wrapper">
            <div class="list-wrapper">
                <div class="list-tittle">
                    <?php if($genre_handler->isApproved){echo "<a></a>";} ?>
                    <h2> Žánry </h2>
                    <?php $genre_handler->change_sorting_labels() ?>
                </div>
                <div class="list">
                    <?php $genre_handler->fill_list_with_items("filter_name"); ?>
                </div>
            </div>
        </div>
        <div class="main-list-wrapper">
            <div class="list-wrapper">
                <div class="list-tittle">
                    <?php if($user_handler->isApproved){echo "<a></a>";} ?>
                    <h2> Uživatelé </h2>
                    <?php $user_handler->change_sorting_labels() ?>
                </div>
                <div class="list">
                    <?php $user_handler->fill_list_with_items("filter_name"); ?>
                </div>
            </div>
        </div>


    <footer class="footer">

        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>

</body>



</html>