<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

    include "php-server/session-handler.php";
    include "php-server/entity-manager.php";
    include "php-server/movie-window-handler.php";
    include "php-server/style-handler.php";
    include "php-server/header_handler.php";


    // set up session   
    session_set_up();

    //get current page name
    $curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 

    //set variables for movie handler
    $path = "data/movies.json";
    $website = "movie";
    $target_column = "id";

    //movie handler
    $handler = new movie_window_handler($path = $path, $curr_website = $website, $target_column = $target_column);
    $handler->validate_GET_args(["id"]);
    $handler->init_data();

    //functions to get entity
    function get_genre($curr_movie, $name){
        return (new entity_manager)->merge_entity("data/genres.json", "name", $name);
    }

    function get_user($curr_movie, $name){
        return (new entity_manager)->merge_entity("data/users.json", "id", $name);
    }


?>

<!DOCTYPE html>
<html lang="cs">

    <head>
        <meta charset="UTF-8">
        <script src="js-scripts/delete-movie.js"></script>
        <link rel="stylesheet" href="css-styles/header-style.css"/>
        <link rel="stylesheet" href="css-styles/body-style.css"/>
        <link rel="stylesheet" href="css-styles/box-style.css"/>
        <link rel="stylesheet" href="css-styles/footer-style.css">
        <?php get_style_file() ?>
        <link rel="stylesheet" media="print" href="css-styles/print/box.css"/>
        <title>movie</title>
    </head>
<body>
    <?php generate_header($curPageName);?>
    <main class="main-box-wrapper">

        <?php $handler->get_left_link() ?>
            <div class="box-switch-wrapper">
            <img class="box-img-switch" alt="switch-left" src="resources/left.png">
            </div>
        </a>
        <div class="box-wrapper">
            <div id="movie-title" class="name-icons-wrapper">
                <div class="box-item box-name">
                    <?php $handler->get_content("name")?>
                </div>
                <div class="box-icons-wrapper">
                        <?php $handler->generate_like_buttons() ?>
                </div>
            </div >
            <div id="movie-genre" class = "box-items">
                <span  class="box-item" >
                    Žánr
                </span>
                <div class="box-item-field">
                    <?php $handler->get_content("genre","genre.php?sort=a&page=1&id=", "get_genre") ?>
                </div>
            </div>
            <div id="movie-actors" class = "box-items">
                <span  class="box-item" >
                    Herci
                </span>
                <div class="box-item-field">
                    <?php $handler->get_content("actors"); ?>
                </div>
            </div>

            <!-- ZDE udelat zda videl nebo nevidel na zaklade seen v movie href na tuto stranku  -->
            <div id="movie-user-seen" class = "box-items">
                <span   class="box-item movie-content" >
                    Líbilo se mi
                </span>
                <?php $handler->generate_liked() ?>
            </div>

            <div id="movie-seen" class = "box-items">
                <span   class="box-item movie-content" >
                    Líbilo se dalším
                </span>
                <div class="box-item-field">
                    <?php $handler->get_content("seen","user.php?id=", "get_user") ?>
                </div>
            </div>



            <div id="movie-content" class = "box-items">
                <span   class="box-item movie-content" >
                    Obsah
                </span>
                <div class="box-item-field"> 
                    <?php $handler->get_content("content")?>
                </div>
            </div>
        </div>

        <?php $handler->get_right_link() ?>
            <div class="box-switch-wrapper">
                <img class="box-img-switch" alt="switch-right" src="resources/right.png">
            </div>
        </a>

    </main>

    <footer class="footer">
        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>
</body>
</html>