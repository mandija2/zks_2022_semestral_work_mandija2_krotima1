<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php


include "php-server/session-handler.php";
include "php-server/controller.php";
include "php-server/style-handler.php";
include "php-server/header_handler.php";

// set up session
session_set_up();

//get current page name
$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 

//set variable for controller
$target_website = "rem_movie.php";

//set controllers
$delcont = new delete_movie_controller(["name"], $target_website);

$delcont->validate();

function print_succes_remove(){
    if(isset($_GET["success"])){
        echo "<p id= 'remove-message' class='success'>".htmlspecialchars($_GET["success"])."</p>";
    }elseif(isset($_GET["error"])){
        echo "<p id= 'remove-message' class='error'>".htmlspecialchars($_GET["error"])."</p>";
    }else{
        echo '<p id="remove-message"> </p>';
    }
}

function print_succes_add(){
    if(isset($_GET["success"])){
        echo "<p id= 'add-message' class='success'>".htmlspecialchars($_GET["success"])."</p>";
    }elseif(isset($_GET["error"])){
        echo "<p id= 'add-message' class='error'>".htmlspecialchars($_GET["error"])."</p>";
    }else{
        echo '<p id="add-message"></p>';
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="js-scripts/move-frame.js"></script>
    <script src="js-scripts/rem-movie-controller.js"></script>
    <link rel="stylesheet" href="css-styles/header-style.css"/>
    <link rel="stylesheet" href="css-styles/add-movie.css"/>
    <link rel="stylesheet" href="css-styles/form-right-style.css"/>
    <link rel="stylesheet" href="css-styles/footer-style.css">
    <link rel="stylesheet" href="css-styles/response-style.css"/>
    <?php get_style_file() ?>
    <link rel="stylesheet" media="print" href="css-styles/print/noprint.css"/>
    <title>add-movie</title>
</head>
<body>
    <?php generate_header($curPageName);?>
    <div class="main-form-wrapper">
        <div class="form-wrapper">
            <div class="button-wrapper">
                <a  href="add_movie.php" class="switch-btn btn_class"> Přidat film </a>
                <a  id="btn" class="switch-btn btn_class"> Odebrat film </a>
            </div>
            <form  action="rem_movie.php" method="POST" id= "first_frame" class="input-wrapper">
                <input id="remmovie" type="text" class="input-field name" placeholder="Název" required name="name" value="<?php echo $delcont->get_arg_value("name")?>" pattern="^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9,.!?# ]+$">
                <button id="remove-btn" type="submit" class="submit-btn">Odebrat</button>
                <?php 
                 if($delcont->isApproved){
                    $delcont->print_error_message();
                 }else{
                    print_succes_remove();
                 }

                ?>
            
            </form>
        </div>
    </div>


    <footer class="footer">
        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>

</body>

</html>
