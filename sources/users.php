<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

    include "php-server/session-handler.php";
    include "php-server/list-handler.php";
    include "php-server/style-handler.php";
    include "php-server/header_handler.php";

    // set up session
    session_set_up();

    //get current page name
    $curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 

    //set variables for user list
    $path = "data/users.json";
    $website = "users";
    $target_website = "user";
    $additional_par ="";
    $max_size_per_page = 13;
    $expected_arg = ["page","sort"];

    //user list handler
    $handler = new List_Handler($path = $path, $curr_website=$website, $target_website= $target_website, $target_params="",$additional_params="", $max_size_per_page = $max_size_per_page);
    $ret = $handler->validate_GET_args($expected_arg);

    //filter out admin
    function filter_admin($data){
        function not_amin($var){
                return !isset($var["admin"]);
        }
        return array_filter($data,"not_amin");
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css-styles/header-style.css"/>
        <link rel="stylesheet" href="css-styles/list-style.css">
        <link rel="stylesheet" href="css-styles/body-style.css">
        <link rel="stylesheet" href="css-styles/footer-style.css">
        <?php get_style_file() ?>
        <link rel="stylesheet" media="print" href="css-styles/print/list.css"/>
        <title>Users</title>
    </head>

<body>
    <?php generate_header($curPageName);?>
    <main class="list-wrapper">
        <div class="list-tittle">
            <?php if($handler->isApproved){echo "<a></a>";} ?>
            <h2> Uzivatele </h2>
            <?php $handler->change_sorting_labels() ?>
        </div>
        <div id="list">
            <?php
                $handler->fill_list_with_items("filter_admin");
            ?>
        </div>
    </main>

    <footer class="footer">
        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>


</body>
</html>