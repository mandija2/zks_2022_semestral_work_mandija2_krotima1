<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

    include "php-server/session-handler.php";
    include "php-server/entity-manager.php";
    include "php-server/user-window-handler.php";
    include "php-server/style-handler.php";
    include "php-server/header_handler.php";
 
    // set up session
    session_set_up();

    //get current page name
    $curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 

    //set variables for user handler
    $path = "data/users.json";
    $website = "user";
    $target_column = "id";

    $handler = new user_window_handler($path = $path, $curr_website = $website, $target_column = $target_column);
    $handler->validate_GET_args(["id"]);
    $handler->init_data();


    //functions return entity
    function get_like_movie($curr_user, $name){
        return (new entity_manager)->merge_entity("data/movies.json", "id", $name);
    }

    function get_dislike_movie($curr_user, $name){
        return (new entity_manager)->merge_entity("data/movies.json", "id", $name);
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <script src="js-scripts/delete-account.js"></script>
        <link rel="stylesheet" href="css-styles/header-style.css"/>
        <link rel="stylesheet" href="css-styles/body-style.css"/>
        <link rel="stylesheet" href="css-styles/box-style.css"/>
        <link rel="stylesheet" href="css-styles/footer-style.css">
        <?php get_style_file() ?>
        <link rel="stylesheet" media="print" href="css-styles/print/box.css"/>
        <title>user</title>
    </head>
<body>
    <?php generate_header($curPageName);?>

    <main class="main-box-wrapper">
        <?php $handler->get_left_link() ?>
            <div class="box-switch-wrapper">
            <img class="box-img-switch" alt="switch-left" src="resources/left.png">
            </div>
        </a>
        <div class="box-wrapper">
            <div id="user-name" class="name-icons-wrapper">
                <div class="box-item box-name">
                    <?php $handler->get_content("name")?>
                </div>
                <div class="box-icons-wrapper">
                    <?php $handler->generate_delete_account_button() //user handler ?>
                </div>
            </div >
            <div id="user-best-genre" class = "box-items">
                <span  class="box-item" >
                    Nej žánr
                </span>
                <div class="box-item-field">
                    <?php $handler->get_favourite_genre()?>
                </div>
            </div>
            <?php $handler->get_personal_info()?>
            <div id="user-seen" class = "box-items">
                <span   class="box-item movie-content" >
                    Líbilo se mi
                </span>
                <div class="box-item-field">
                    <?php $handler->get_content("like","movie.php?id=", "get_like_movie") ?>
                </div>
            </div>

            <div id="user-not-seen" class = "box-items">
                <span   class="box-item movie-content" >
                    Nelíbilo se mi
                </span>
                <div class="box-item-field">
                    <?php $handler->get_content("dislike","movie.php?id=", "get_dislike_movie") ?>
                </div>
            </div>
        </div>

        <?php $handler->get_right_link() ?>
            <div class="box-switch-wrapper">
                <img class="box-img-switch" alt="switch-right" src="resources/right.png">
            </div>
        </a>

    </main>

    <footer class="footer">
        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>
</body>
</html>