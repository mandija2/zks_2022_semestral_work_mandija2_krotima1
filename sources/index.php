<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php

include "php-server/session-handler.php";
include "php-server/style-handler.php";
include "php-server/list-handler.php";
include "php-server/header_handler.php";

// set up session
session_set_up();

//get current page name
$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);  

//set variables
$path_movie = "data/movies.json";
$website = "index";
$target_website_movie = "movie";
$additional_par ="";
$max_size_per_page = 5;
$expected_arg = [];

$path_genre = "data/genres.json";
$target_website_genre = "genre";
$target_par_genre ="&sort=a&page=1";

//create list handlers
$movie_handler = new List_Handler($path = $path_movie, $curr_website=$website, $target_website= $target_website_movie, $target_params="",$additional_params="", $max_size_per_page = $max_size_per_page);
$movie_handler->validate_GET_args($expected_arg);

$genre_handler = new List_Handler($path = $path_genre, $curr_website=$website, $target_website = $target_website_genre, $target_params=$target_par_genre,$additional_params="", $max_size_per_page = $max_size_per_page);
$genre_handler->validate_GET_args($expected_arg);

//filter movies, return 5 best movies
function movie_filter($data){
    function cmp_m($a, $b){
        $key = "seen";
        return sizeof($a[$key ]) < sizeof($b[$key]);
    }
    usort($data, "cmp_m");
    $max_size = sizeof($data) < 5? sizeof($data) : 5;
    return array_slice($data, 0,$max_size); 
}

//filter genres, return 5 best genres
function genre_filter($data){
    function cmp_g($a, $b){
        $key = "likes";
        return $a[$key] < $b[$key];
    }
    usort($data, "cmp_g");
    $max_size = sizeof($data) < 5? sizeof($data) : 5;
    return array_slice($data, 0,$max_size); 
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="js-scripts/header.js"></script>
    <link rel="stylesheet" href="css-styles/header-style.css"/>
    <link rel="stylesheet" href="css-styles/body-index-style.css"/>
    <link rel="stylesheet" href="css-styles/list-style.css">
    <link rel="stylesheet" href="css-styles/footer-style.css">
    <?php get_style_file() ?>
    <link rel="stylesheet" media="print" href="css-styles/print/index.css"/>
    <title>Filmator</title>
</head>

<body>
   <?php generate_header($curPageName);?>
    <div class="background">
        <div class="first-section">
            
                <div class="slider-frame">
                    <div class="main-images-wrapper">
                        <div class="img-container">
                            <img alt="intro1" src="resources/intro1.jpg">
                        </div>
                        <div class="img-container">
                            <img alt="intro2" src="resources/intro2.jpg">
                        </div>
                        <div class="img-container">
                            <img alt="intro3" src="resources/intro3.jpg">
                        </div>
                        <div class="img-container">
                            <img alt="intro4" src="resources/intro4.jpg">
                        </div>
                        <div class="img-container">
                            <img alt="intro5" src="resources/intro5.jpg">
                        </div>
                    </div>
                </div>
            
            <div class="main-favorite-movie-wrapper">
                <div class="main-list-wrapper">
                    <div class="list-wrapper">
                        <div class="list-tittle"><h2> TOP FILMY </h2></div>
                        <div class="list">
                            <?php $movie_handler->fill_list_with_items("movie_filter"); ?>
                        </div>
                    </div>
                </div>
                <div class="main-list-wrapper">
                    <div class="list-wrapper">
                        <div class="list-tittle"><h2> TOP ZANRY </h2></div>
                        <div class="list">
                            <?php $genre_handler->fill_list_with_items("genre_filter"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">

        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>

</body>



</html>