<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php


    include "php-server/session-handler.php";
    include "php-server/controller.php";
    include "php-server/style-handler.php";
    include "php-server/header_handler.php";

    // set up session   
    session_set_up();


    //get current page name
    $curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 

    //set variable for login controller
    $target_website = "index.php?username=";

    //create controllers
    $regcont = new register_controller(["name","email","number","password","apassword"], $target_website);

    if ($regcont->isApproved){
        $regcont->validate();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="js-scripts/move-frame.js"></script>
    <script src="js-scripts/register-controller.js"></script>
    <link rel="stylesheet" href="css-styles/header-style.css"/>
    <link rel="stylesheet" href="css-styles/body-style.css"/>
    <link rel="stylesheet" href="css-styles/form-right-style.css"/>
    <link rel="stylesheet" href="css-styles/footer-style.css">
    <link rel="stylesheet" href="css-styles/response-style.css"/>
    <?php get_style_file() ?>
    <link rel="stylesheet" media="print" href="css-styles/print/noprint.css"/>
    <title>login</title>
</head>
<body>
    <?php generate_header($curPageName);?>
    <div class="main-form-wrapper">
        <div class="form-wrapper">
            <div class="button-wrapper">
                <a  href="login.php" class="switch-btn btn_class"> Přihlásit se </a>
                <a  id="btn" class="switch-btn btn_class"> Registrovat se </a>
            </div>
            <form action="register.php" method="POST" id= "first_frame" class="input-wrapper">
                <label for="regname" class="label-name"> Username </label>  
                <input id="regname" type="text" class="input-field name"  placeholder="Username*" required name="name" value="<?php echo $regcont->get_arg_value("name")?>" pattern="^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9]*$">
                <label for="regemail" class="label-name"> Email </label>
                <input id="regemail" type="email" class="input-field email" placeholder="Email*" required name="email" value="<?php echo $regcont->get_arg_value("email")?>" >
                <label for="regnum" class="label-name"> Telefon </label>
                <input id="regnum" type="tel" class="input-field email" placeholder="Tel. číslo*" required name="number" value="<?php echo $regcont->get_arg_value("number")?>" pattern="^[0-9]*$">
                <label for="regpass" class="label-name"> Heslo </label>
                <input id="regpass" type="password" class="input-field password" placeholder="Heslo*" required name="password" pattern="^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9]*$">
                <label for="regapass" class="label-name"> Znovu heslo </label>
                <input id="regapass" type="password" class="input-field apassword" placeholder="Znovu heslo*" required  name="apassword" pattern="^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9]*$">
                <button id="register-btn" type="submit" class="submit-btn">Registrovat</button>
                <?php 
                    if ($regcont->isApproved){
                        $regcont->print_error_message();
                    }else{
                        echo '<p id= "error-message"></p>';
                    }
                ?>
            </form>
        </div>
    </div>

    <footer class="footer">
        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>


</body>

</html>
