//Global variable
var MINPassLength = 4;

/*
  onload
*/
window.onload = function(){
    input_listener_login();
    //input_listener_register();
    //init_tab_focus();
    init_submit_controller_log();
}


/**
 *  validates given text by user.
 *      e:       submit event
 *      what:    string indicating what is going to be validated
 */
function validate(e, what){
    if (what == "registration"){
        result = control_register();
        if(result != true){
            alert("Máš tam chybu: " + result);
            e.preventDefault();
            // window.history.back();
            console.log("Registration is not successfull");
            return;
        }else{
            //alert("Úspěšně jsi se registroval!");
            console.log("Registration is successfull");
            return;
        }
    }else if (what == "login"){
        result = control_login();
        if(result != true){
            alert("Nelze tě příhlásit: " + result);
            e.preventDefault();
            // window.history.back();
            console.log("Login is not successfull");
            return;
        }else{
            //alert("Úspěšně jsi se přihlásil");
            console.log("Login is successfull");
            return;
        } 
    }
}


/*
    Checks all input fields in the login form and
    if they are incorrect it returns error-message.
    otherwise it returns True.

*/

function control_login(){
    var name = document.getElementById("logname").value;
    var password = document.getElementById("logpass").value;
    if (name == "" || password == ""){
        return "Doplňte všechna pole!";
    }
    if (!onlyLettersAndDigits(name)){
        return "Jméno musí obsahovat pouze číslice nebo písmena!";
    }
    if (password.length <4){
        return "Heslo musí mít alespoň 4 znaky!";
    }
    return true;
}

/*
    Checks all input fields in the register form and
    if they are incorrect it returns error-message.
    otherwise it returns True.

*/
function control_register(){
    var name = document.getElementById("regname").value;
    var email = document.getElementById("regemail").value;
    var phoneNum = document.getElementById("regnum").value;
    var password = document.getElementById("regpass").value;
    var apassword = document.getElementById("regapass").value;
    if (name == "" || email == "" || password == "" || apassword== ""){
        return  "Doplňte všechna pole!";
    }
    if(!onlyLettersAndDigits(name)){
        return "Jméno musí obsahovat pouze číslice nebo písmena!";
    }
    if(name.length < 4){
        return "Jméno musí obsahovat alespoň 4 znaky!";
    }
    if (!validateEmail(email)){
        return "Email není zadán správně, musí být jako _@_.__!";
    }
    if (!onlyDigits(phoneNum)){
        return "Telefonní číslo musí obsahovat pouze číslice";
    }
    if (phoneNum.length != 9 ){
        return "Telefonní číslo musí mít přesně 9 číslic";
    }
    if (password.length <4){
        return "Heslo musí mít alespoň 4 znaky!";
    }
    if (password != apassword){
        return "Hesla se neshoduji!";
    }
    return true;
}


/*
    Checks given string whether matches the email structure. 
    - email:    string
    - return:   boolean- True if it matches, False otherwise
*/
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

/*
    Checks given string whether contains only digits or letters. 
    - test:    string
    - return:  boolean- True if it matches, False otherwise
*/
function onlyLettersAndDigits(test) {
    return test.match("^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9]*$");
  }

function onlyDigits(test){
    return test.match("^[0-9]*$");
}


/* CHECK INPUT DURING TYPING */

/* 
    writes message to the innerText and changes style class of an element found by given id
*/
function set_error_message(id, message){
    var er = document.getElementById(id);
    er.style.visibility = "visible";
    er.innerText = message;
    er.classList.remove("success")
    er.classList.add("error");
}
/*
    writes message to the innerText and changes style class of an element found by given id
*/
function set_success_message(id,message){
    var er = document.getElementById(id);
    er.style.visibility = "visible";
    er.innerText = message;
    er.classList.remove("error");
    er.classList.add("success");
}
/*
    sets style visibility hidden of an element found by given id
*/
function clear_message(id){
    var er = document.getElementById(id);
    er.style.visibility = "hidden";
}



/**
 *  Adds eventListener to submit buttons
 *  
 */

 function init_submit_controller_reg(){
    var regBTN = document.getElementById("register-btn");



    regBTN.addEventListener("click", 
        function(e){
            console.log("Register button is clicked");
            validate(e, "registration");
            
        }
    );

    regBTN.addEventListener("submit", 
        function(e){
            validate(e, "registration");
            
        }
    );

    console.log("Submit controller initialized.");
}

function init_submit_controller_log(){
    var logBTN = document.getElementById("login-btn");


    logBTN.addEventListener("click",
        function(e){
            console.log("Login button is clicked");
            validate(e, "login");
            
        }
    );

    logBTN.addEventListener("submit", 
        function(e){
            validate(e, "login");
            
        }
    );

    console.log("Submit controller initialized.");
}




/* 
    input listener for login form.
    - checks the user's input text whether it is correct during typing.
*/
function input_listener_login(){
    var inputName = document.getElementById("logname");
    var inputPass = document.getElementById("logpass");
    var loginButton = document.getElementById("login-btn");

    // set timeoutId as kind of a global variable but in the scope of this function to be
    // simply used in methods defined below.
    var timeOutId = 0;
    

    /*
        Simple methods for checking input 
    */
    inputName.addEventListener("input",  function(){
            clearInterval(timeOutId);
            clear_message("login-error-message")
            if (!onlyLettersAndDigits(this.value)){
                timeOutId = setTimeout(set_error_message, 500,"login-error-message", "Jméno musí obsahovat pouze číslice a písmena.");
            }
    });

    inputPass.addEventListener("input", function(){
        clearInterval(timeOutId);
        clear_message("login-error-message")
        if (this.value.length < MINPassLength){
            timeOutId = setTimeout(set_error_message, 500,"login-error-message", "Heslo musí být alespoň 4 znaky dlouhé!");
        }else{
            timeOutId = setTimeout(set_success_message, 1000,"login-error-message", "Heslo vypadá OK!");
        }
    });

    loginButton.onmouseover = function(){
        var result = control_login();
        if ( result != true ){
            timeOutId = setTimeout(set_error_message, 500,"login-error-message", result);
        }else{
            timeOutId = setTimeout(set_success_message, 1000,"login-error-message", "Všechna pole vyplněna správně!");

        }
    };
    console.log("Input login listener initialized.");
}

/* 
    input listener for register form.
    - checks the user's input text whether it is correct during typing.
*/
function input_listener_register(){
    //get elements from html
    var regFrame = document.getElementById("second_frame");
    var inputName = document.getElementById("regname");
    var inputEmail = document.getElementById("regemail");
    var inputPhoneNum = document.getElementById("regnum");
    var inputPass = document.getElementById("regpass");
    var inputAPass = document.getElementById("regapass");
    var registerButton = document.getElementById("register-btn");
    
    // set timeoutId as kind of a global variable but in the scope of this function to be
    // simply used in methods defined below.
    var timeOutId = 0;


    /*
        Simple methods for checking input 
    */

    registerButton.onmouseover = function(){
        var result = control_register();
        if ( result != true ){
            timeOutId = setTimeout(set_error_message, 500,"error-message", result);
        }else{
            timeOutId = setTimeout(set_success_message, 1000,"error-message", "Všechna pole vyplněna správně!");

        }
    };

    inputName.oninput = function(){
        clearInterval(timeOutId);
        clear_message("error-message")
        if (!onlyLettersAndDigits(this.value)){
            timeOutId = setTimeout(set_error_message, 1000,"error-message", "Jméno musí obsahovat pouze číslice a písmena.");
        }
    };


    inputEmail.oninput = function(){
        clearInterval(timeOutId);
        clear_message("error-message")

        if (!validateEmail(this.value)){
            timeOutId = setTimeout(set_error_message, 1000,"error-message", "Email musí být jako _@_.__!");
        }else{
            timeOutId = setTimeout(set_success_message, 1000,"error-message", "Email zadán správně!");
        }

    };

    inputPhoneNum.oninput = function(){
        if (!onlyDigits(this.value)){
            this.value = this.value.slice(0,-1);
        }
        clearInterval(timeOutId);
        clear_message("error-message")
        if (this.value.length != 9){
            timeOutId = setTimeout(set_error_message, 1000,"error-message", "Telefonní číslo musí mít přesně 9 číslic");
        }else{
            timeOutId = setTimeout(set_success_message, 1000,"error-message", "Telefonní číslo zadáno správně!");
        }
    }
    
    inputAPass.oninput = function(){
        clearInterval(timeOutId);
        clear_message("error-message")

        if (this.value.length < MINPassLength){
            timeOutId = setTimeout(set_error_message, 1000,"error-message", "Heslo musí být alespoň 4 znaky dlouhé!");
        }
        else{
            if (inputPass.value != this.value){
                timeOutId = setTimeout(set_error_message, 1000,"error-message", "Hesla se neshodují!");
            }
            else if (this.value == inputPass.value){
                timeOutId = setTimeout(set_success_message, 1000,"error-message", "Hesla se shodují!");
            }
        }
    };

    inputPass.oninput = function(){
        clearInterval(timeOutId);
        clear_message("error-message")
        
        if (this.value.length < MINPassLength){
            timeOutId = setTimeout(set_error_message, 1000,"error-message" , "Heslo musí být alespoň 4 znaky dlouhé!");
        }
        else{ 
            if (inputAPass.value != "" && this.value != inputAPass.value){
                timeOutId = setTimeout(set_error_message, 1000,"error-message", "Hesla se neshodují!");
            }
            else if (this.value == inputAPass.value){
                timeOutId = setTimeout(set_success_message, 1000,"error-message", "Hesla se shodují!");
            }
        }
    };
    console.log("Input register listener initialized.");
}