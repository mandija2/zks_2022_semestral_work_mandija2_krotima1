function onLoad() {
    const clearFormButton = document.getElementById("delete");
    clearFormButton.addEventListener("click", function(e) {confirmDeleteAccountt(e)});
  }
  
  function confirmDeleteAccountt(e) {
    const isConfirmed = confirm("Opravdu chceč odstranit účet?");
    if (!isConfirmed) {
        e.preventDefault();
    }
  }
  
  window.addEventListener("load", onLoad);
  