
/*
    Sets the tabindex all elements in the first frame,
    function usually called in window.onload
*/
function init_tab_focus(){
    var f_frame = document.getElementById("first_frame");
    var s_frame = document.getElementById("second_frame");
    /*CALLING FUNCTION FROM MOVE-FRAME.JS */
    setTabFocus(f_frame,s_frame);
    console.log("TabIndex focus initialized!")
}


/* 
    pushes first frame away and moves the second.
*/
function second_frame(){
    var f_frame = document.getElementById("first_frame");
    var s_frame = document.getElementById("second_frame");
    var btn = document.getElementById("btn");
    f_frame.style.left = "-400px"
    s_frame.style.left = "50px"
    btn.style.left = "150px"

    setTabFocus(s_frame,f_frame);
    console.log("Second frame is on the focus.")

}

/* 
    pushes second frame away and moves the first.
*/
function first_frame(){
    var f_frame = document.getElementById("first_frame");
    var s_frame = document.getElementById("second_frame");
    var btn = document.getElementById("btn");
    f_frame.style.left = "50px"
    s_frame.style.left = "450px"
    btn.style.left = "0px"

    setTabFocus(f_frame,s_frame);
    console.log("First frame is on the focus.")
}

/**
 *   Sets tabulator index all the children of focus_frame
 *   and removes tabindex to all children in fram
 *       focus_frame   : div element, to be added tabIndex
 *       frame         : div element to be removed tabIndex
 */

function setTabFocus(focus_frame, frame){
    frame.childNodes.forEach(t => t.tabIndex = -1);
    for (var i = 0; i < focus_frame.childNodes.length; i++){
        focus_frame.childNodes[i].tabIndex = i;
    }
}