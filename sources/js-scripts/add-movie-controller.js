window.onload = function(){
    //init_tab_focus();
    init_add_listener();
    //init_remove_listener();
    init_submit_controller_add();
}

/*
    Checks all input fields in the add-movie form and
    if they are incorrect it returns error-message.
    otherwise it returns True.

*/

function control_add(){
    var name = document.getElementById("addmovie").value;
    var genre = document.getElementById("addgenre").value;
    var actors = document.getElementById("addactors").value;
    var content = document.getElementById("addcontent").value;

    if (name =="" || actors =="" || genre==""){
        return "Doplňte všechna pole!";
    }
    if (!onlyLettersDigitsAndMarks(name)){
        return "Název smí obsahovat pouze písmena, čísla, nebo !?.,";
    }
    if (!onlyLettersGenre(genre)){
        return "Žánr smí obsahovat pouze písmena, čárku pro oddělení a mezery";
    }
    if (!onlyLettersDigitsAndMarks(actors)){
        return "Herci smí obsahovat pouze písmena, čísla, nebo !?.,";
    }
    return true;
}


/*
    Checks all input fields in the remove-movie form and
    if they are incorrect it returns error-message.
    otherwise it returns True.

*/
function control_remove(){
    var name = document.getElementById("remmovie").value;
    if (name == "" ){
        return  "Doplňte název!";
    }
    if (!onlyLettersDigitsAndMarks(name)){
        return "Název smí obsahovat pouze písmena, čísla, nebo !?.,";
    }
    return true;
}

/*
    Checks given string whether contains only digits or letters. 
    - test:    string
    - return:  boolean- True if it matches, False otherwise
*/
function onlyLettersDigitsAndMarks(test) {
    return test.match("^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9,.!?# ]+$");
  }


/*
    Checks given string whether contains only letters and comma to separate genres. 
    - test:    string
    - return:  boolean- True if it matches, False otherwise
*/
function onlyLettersGenre(test) {
    return test.match("^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z, ]+$");
  }


  /* CHECK INPUT DURING TYPING */

/* 
    writes message to the innerText and changes style class of an element found by given id
*/
function set_error_message(id, message){
    var er = document.getElementById(id);
    er.style.visibility = "visible";
    er.innerText = message;
    er.classList.remove("success")
    er.classList.add("error");
}
/*
    writes message to the innerText and changes style class of an element found by given id
*/
function set_success_message(id,message){
    var er = document.getElementById(id);
    er.style.visibility = "visible";
    er.innerText = message;
    er.classList.remove("error");
    er.classList.add("success");
}
/*
    sets style visibility hidden of an element found by given id
*/
function clear_message(id){
    var er = document.getElementById(id);
    er.style.visibility = "hidden";
}



/* 
    input listener for remove-movie form.
    - checks the user's input text whether it is correct during typing.
*/
function init_remove_listener(){
    var remMovie = document.getElementById("remmovie");
    var removeBTN = document.getElementById("remove-btn");

    // set timeoutId as kind of a global variable but in the scope of this function to be
    // simply used in methods defined below.
    var timeOutId = 0;

    remMovie.addEventListener("input",  function(){
        clearInterval(timeOutId);
        clear_message("remove-message")
        if (!onlyLettersDigitsAndMarks(this.value)){
            timeOutId = setTimeout(set_error_message, 500,"remove-message", "Název smí obsahovat pouze písmena, čísla, nebo !?.,");
        }
    });

    removeBTN.onmouseover = function(){
        var result = control_remove();
        if ( result != true ){
            timeOutId = setTimeout(set_error_message, 500,"remove-message", result);
        }else{
            timeOutId = setTimeout(set_success_message, 1000,"remove-message", "Všechna pole vyplněna správně!");

        }
    };

    console.log("Input remove-movie listener initialized.");
}


// 


/* 
    input listener for add-movie form.
    - checks the user's input text whether it is correct during typing.
*/
function init_add_listener(){

    var addMovie = document.getElementById("addmovie");
    var addGenre = document.getElementById("addgenre");
    var addActors = document.getElementById("addactors");
    var addBTN = document.getElementById("add-btn");


    // set timeoutId as kind of a global variable but in the scope of this function to be
    // simply used in methods defined below.
    var timeOutId = 0;

    
    addMovie.addEventListener("input",  function(){
        clearInterval(timeOutId);
        clear_message("add-message")
        if (!onlyLettersDigitsAndMarks(this.value)){
            timeOutId = setTimeout(set_error_message, 500,"add-message", "Název smí obsahovat pouze písmena, čísla, nebo !?., a mezery");
        }
    });

    addGenre.addEventListener("input",  function(){
        clearInterval(timeOutId);
        clear_message("add-message")
        if (!onlyLettersGenre(this.value)){
            timeOutId = setTimeout(set_error_message, 500,"add-message", "Žánr smí obsahovat pouze písmena, nebo , a mezery");
        }
    });

    addActors.addEventListener("input",  function(){
        clearInterval(timeOutId);
        clear_message("add-message")
        if (!onlyLettersDigitsAndMarks(this.value)){
            timeOutId = setTimeout(set_error_message, 500,"add-message", "Herci smí obsahovat pouze písmena, čísla, nebo !?., a mezery");
        }
    });

    addBTN.onmouseover = function(){
        var result = control_add();
        if ( result != true ){
            timeOutId = setTimeout(set_error_message, 500,"add-message", result);
        }else{
            timeOutId = setTimeout(set_success_message, 1000,"add-message", "Všechna pole vyplněna správně!");

        }
    };
    console.log("Input add-movie listener initialized.");




}


/**
 *  Adds eventListener to submit buttons
 * 
 */

 function init_submit_controller_add(){
    var addTN = document.getElementById("add-btn");



    addTN.addEventListener("click", 
        function(e){
            console.log("Add movie button is clicked");
            validate(e, "add");
            
        }
    );

    addTN.addEventListener("submit", 
        function(e){
            validate(e, "add");
            
        }
    );

    console.log("Submit controller initialized.");
}


/**
 *  Adds eventListener to submit buttons
 * 
 */

 function init_submit_controller_rem(){
    var remBTN = document.getElementById("remove-btn");



    remBTN.addEventListener("click",
        function(e){
            console.log("Remove movie button is clicked");
            validate(e, "remove");
            
        }
    );

    remBTN.addEventListener("submit", 
        function(e){
            validate(e, "remove");
            
        }
    );

    console.log("Submit controller initialized.");
}



/**
 *  validates given text by user.
 *      e:       submit event
 *      what:    string indicating what is going to be validated
 */
 function validate(e, what){
    if (what == "remove"){
        result = control_remove();
        if(result != true){
            alert("Máš tam chybu: " + result);
            e.preventDefault();
            // window.history.back();
            console.log("Removal is not successfull");
            return;
        }else{
            //alert("Úspěšně jsi film odebral.");
            console.log("Removal is successfull");
            return;
        }
    }else if (what == "add"){
        result = control_add();
        if(result != true){
            alert("Nelze přidat film: " + result);
            e.preventDefault();
            // window.history.back();
            console.log("addition is not successfull");
            return;
        }else{
            //alert("Úspěšně jsi přidal film!");
            console.log("addition is successfull");
            return;
        } 
    }
}

