<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php


    include "php-server/entity-manager.php";
    include "php-server/list-handler.php";
    include "php-server/style-handler.php";
    include "php-server/header_handler.php";
 
    // set up session
    session_set_up();

    //get current page name
    $curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 


    //check expected argument
    function check_get_ID(){
        return isset($_GET["id"]);
    }

    $isApprovedID = check_get_ID();

    //set variables for list handler
    $path = "data/movies.json";
    $website = "genre";
    $additional_par =  $isApprovedID ? "&id=".htmlspecialchars($_GET["id"]) : "";
    $target_website = "movie";
    $max_size_per_page = 13;
    $expected_arg = ["page", "sort", "id"];

    //create instance of list handler
    $handler = new List_Handler($path = $path, $curr_website=$website, $target_website= $target_website, $target_params="", $additional_params = $additional_par, $max_size_per_page = $max_size_per_page);
    $ret = $handler->validate_GET_args($expected_arg);

    // returns current genre
    function get_genre(){        
        if (is_numeric($_GET["id"])){
            $genre_id = (int)$_GET["id"];
            $genre = (new entity_manager)->merge_entity("data/genres.json","id", $genre_id);
            if (sizeof($genre)==0){
                return null;
            }
            return $genre;
        
        }
        return null;
    }

    // returns movies occurring in the current genre
    function filter_data($data){
        $genre = get_genre();
        if ($genre!=null){
            return array_filter($data, 
                        function ($movie) use ($genre){
                                    return in_array($movie["id"],$genre["movies"]);
                                }
                        );
        }else{
            return $data;
        }
    }



?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css-styles/header-style.css"/>
        <link rel="stylesheet" href="css-styles/list-style.css">
        <link rel="stylesheet" href="css-styles/body-style.css">
        <link rel="stylesheet" href="css-styles/footer-style.css">
        <?php get_style_file() ?>
        <link rel="stylesheet" media="print" href="css-styles/print/list.css"/>
        <title>Zanr</title>
    </head>

<body>
    <?php generate_header($curPageName);?>
    <main class="list-wrapper">
        <div class="list-tittle">
            <?php if($handler->isApproved){echo "<a></a>";} ?>
            <h2> 
                <?php if(($gg = get_genre())!=null){echo $gg["name"];}else{echo "Filmy";}?> 
            </h2>
            <?php $handler->change_sorting_labels() ?>
        </div>
        <div id="list">
            <?php
                $handler->fill_list_with_items("filter_data");
            ?>
        </div>
   </main>

    <footer class="footer">
        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>


</body>



</html>