<?php ini_set("display_errors",1); ?>
<?php ini_set("display_startup_errors",1); ?>
<?php error_reporting(E_ALL); ?>
<?php


    include "php-server/session-handler.php";
    include "php-server/controller.php";
    include "php-server/style-handler.php";
    include "php-server/header_handler.php";

    // set up session   
    session_set_up();


    //get current page name
    $curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 

    //set variable for login controller
    $target_website = "index.php?username=";

    //create controllers
    $logcont = new login_controller(["name","password"], $target_website);

    if ($logcont->isApproved){
        $logcont->validate();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="js-scripts/move-frame.js"></script>
    <script src="js-scripts/login-controller.js"></script>
    <link rel="stylesheet" href="css-styles/header-style.css"/>
    <link rel="stylesheet" href="css-styles/body-style.css"/>
    <link rel="stylesheet" href="css-styles/form-right-style.css"/>
    <link rel="stylesheet" href="css-styles/footer-style.css">
    <link rel="stylesheet" href="css-styles/response-style.css"/>
    <?php get_style_file() ?>
    <link rel="stylesheet" media="print" href="css-styles/print/noprint.css"/>
    <title>login</title>
</head>
<body>
    <?php generate_header($curPageName);?>
    <div class="main-form-wrapper">
        <div class="form-wrapper">
            <div class="button-wrapper">
                <a  id="btn" class="switch-btn btn_class"> Přihlásit se </a>
                <a  href="register.php"  class="switch-btn btn_class"> Registrovat se </a>
            </div>
            <form action="login.php" method="POST"  id="first_frame" class="input-wrapper">
                <label for="logname" class="label-name"> Username </label>
                <input id="logname" type="text" class="input-field name" tabindex="1" placeholder="Username*" required name="name" value="<?php echo $logcont->get_arg_value("name")?>" pattern="^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9]*$">
                <label for="logpass" class="label-name"> Heslo </label>
                <input id="logpass" type="password" class="input-field password" tabindex="2" placeholder="Heslo*" required name="password" pattern="^[ěščřžýáíéóúůďťňĎŇŤŠČŘŽÝÁÍÉÚŮĚÓa-zA-Z0-9]*$">
                <button id="login-btn" type="submit" class="submit-btn" >Přihlásit se</button>
                <?php 
                    if ($logcont->isApproved){
                        $logcont->print_error_message();
                    }else{
                        echo '<p id="login-error-message"></p>';
                    }
                ?>
            </form>
        </div>
    </div>

    <footer class="footer">
        <div class="footer-text">
            Filmator s.r.o
        </div>
        <div class="footer-img">
            <img class="icon" alt="icon-instagram" src="resources/instagram-icone.png">
        </div>
    </footer>


</body>

</html>
